// Command cgitatom polls a cgit atom endpoint once for repo udpates.
//
// This doesn't really belong in this package, but it may be convenient when
// an NNTP server goes down.
//
// $ cp ../../pkg/feed/testdata/01.atom.xml /tmp/fakelatest.xml && \
//   go run . \
//   --save-path /tmp/fakelatest.xml \
//   --cgit-url http://localhost:8888/cgit/cgit.cgi/poke/atom \
//   --irc-chan '#chan' \
//   --irc-host localhost \
//   --irc-port 12345
//

package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/jpneverwas/nntp-crier/internal/transpo"
	"gitlab.com/jpneverwas/nntp-crier/pkg/feed"
)

var flagTemplate string
var flagSavePath string
var flagCgitURL string
var flagIRCcatChan string
var flagIRCcatHost string
var flagIRCcatPort int

func checkArgs() bool {
	flag.StringVar(&flagSavePath, "save-path", "", "Path to last snapshot")
	flag.StringVar(&flagCgitURL, "cgit-url", "", "Like https://foo/cgit/atom")
	flag.StringVar(&flagIRCcatChan, "irc-chan", "", "IRCcat channel name")
	flag.StringVar(&flagIRCcatHost, "irc-host", "", "IRCcat host name")
	flag.IntVar(&flagIRCcatPort, "irc-port", 0, "IRCcat port name")
	// Optional
	flag.StringVar(
		&flagTemplate, "template", feed.DefaultTemplate, "Msg format template",
	)
	flag.Parse()
	// Template can be blank
	missing := flagSavePath == "" ||
		flagCgitURL == "" ||
		flagIRCcatChan == "" ||
		flagIRCcatHost == "" ||
		flagIRCcatPort == 0
	return !missing
}

func main() {
	if !checkArgs() {
		fmt.Fprintln(os.Stderr, "Required option missing")
		os.Exit(1)
	}
	existing, err := feed.LoadLatest(flagSavePath)
	if err != nil {
		panic(err)
	}
	fetched, err := feed.FetchURL(flagCgitURL)
	if err != nil {
		panic(err)
	}
	current, err := feed.LoadFetched(fetched)
	if err != nil {
		panic(err)
	}
	if err = feed.Validate(existing, current); err != nil {
		panic(err)
	}
	todo := feed.GetNew(existing, current)
	if len(todo) == 0 {
		fmt.Println("All caught up; nothing to do")
		os.Exit(0)
	}
	for i, item := range todo {
		if i > 0 {
			time.Sleep(time.Second * 5)
		}
		rendered, err := feed.RenderMessage(flagTemplate, 369, item, current)
		if err != nil {
			panic(err)
		}
		if err := transpo.CatOutput(
			flagIRCcatChan,
			transpo.FmtTCP(flagIRCcatHost, flagIRCcatPort),
			rendered,
		); err != nil {
			if err != nil {
				panic(err)
			}
		}
		fmt.Println("Sent:", string(rendered))
	}
	if err = feed.SaveNewLatest(flagSavePath, fetched); err != nil {
		panic(err)
	}
}
