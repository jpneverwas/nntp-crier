package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"regexp"

	"github.com/antchfx/htmlquery"
	"golang.org/x/net/html"
	"golang.org/x/net/html/charset"
	"golang.org/x/net/proxy"
)

const baseUrl = "https://lists.gnu.org"
const namaUrl = "https://lists.gnu.org/archive/cgi-bin/namazu.cgi"

const usageBlurb = `Usage for %s:
  [-id <id> -list <list> | -file <file>] [-ec <ec>] [-json]

  Params -id and -list go together and can't be combined with -file.

  With -json, allow passing -list and -id (or -file) as json to stdin,
  and output json whenever possible (like when no message is found).

`

type alogger interface {
	Fatal(...interface{})
	Fatalf(string, ...interface{})
}

var logger alogger = log.Default()
var flagMessageId string
var flagListName string
var flagFile string
var flagExitCode int
var flagJson bool

var outputStream io.Writer = os.Stdout

type extraInfo struct {
	Url string `json:"url"`
	Subject string `json:"subject"`
	Date string `json:"date"`
	Summary string `json:"summary"`
}

type errorInfo struct {
	Code int `json:"code"`
	Message string `json:"message"`
}

type notFoundInfo struct {
	Error *errorInfo `json:"error"`
}

type stdinOpts struct {
	List string `json:"list"`
	Id string `json:"id"`
	File string `json:"file"`
}

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(), usageBlurb, os.Args[0],)
	flag.PrintDefaults()
}

func initOpts() {
	if flagMessageId + flagListName + flagFile != "" {
		return
	}
	flag.StringVar(
		&flagMessageId, "id", "", "An e-mail Message-Id",
	)
	flag.StringVar(
		&flagListName, "list", "", "Name of a GNU archives mailing list",
	)
	flag.StringVar(
		&flagFile, "file", "", "Name of HTML file or - to read from stdin",
	)
	flag.IntVar(
		&flagExitCode, "ec", 42, "Desired exit code when message not found",
	)
	flag.BoolVar(
		&flagJson, "json", false, "Print a JSON object with extra info;"+
			" otherwise, just print the URL",
	)
	flag.Usage = usage
	flag.Parse()

	if (flagJson &&
		flagMessageId == "" && flagListName == "" && flagFile == "") {
		if maybe, err := io.ReadAll(os.Stdin); err == nil {
			var maybeOpts stdinOpts
			if err := json.Unmarshal(maybe, &maybeOpts); err == nil {
				os.Args = os.Args[:1]
				if maybeOpts.List != "" {
					os.Args = append(os.Args, "-list", maybeOpts.List)
				}
				if maybeOpts.Id != "" {
					os.Args = append(os.Args, "-id", maybeOpts.Id)
				}
				if maybeOpts.File != "" && maybeOpts.File != "-" {
					os.Args = append(os.Args, "-file", maybeOpts.File)
				}
				flag.Parse()
			}
		}
	}

	if ((flagFile == "" && (flagMessageId == "" || flagListName == "")) ||
		(flagFile != "" && (flagMessageId != "" || flagListName != ""))) {
		flag.Usage()
		os.Exit(1)
	}
}

var referencesPattern = regexp.MustCompile("^References:.+: (\\d+) +[]]$")
var referencesNope = regexp.MustCompile("^References:.+can't open the index")
var msgIdPat = regexp.MustCompile("^<([^>]+)>$")

func getResultCount(doc *html.Node) (int64, error) {
	paras, err := htmlquery.QueryAll(
		doc, "//div[@class='namazu-result-header']/p",
	)
	if err != nil {
		return 0, err
	}
	if len(paras) != 2 {
		return 0, err
	}
	resText := htmlquery.InnerText(paras[0])
	subs := referencesPattern.FindStringSubmatch(strings.TrimSpace(resText))
	if len(subs) == 0 &&
		referencesNope.MatchString(strings.TrimSpace(resText)) {
		return 0, nil
	}
	if len(subs) != 2 {
		return 0, fmt.Errorf("Trouble parsing results: %s", resText)
	}
	found := subs[1]
	count, err := strconv.ParseInt(found, 10, 64)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func findFirstA(doc *html.Node) (*html.Node, error) {
	a, err := htmlquery.Query(
		doc, "//dl/dt/strong/a@href",
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}

func findDate(doc *html.Node) (*html.Node, error) {
	em, err := htmlquery.Query(
		doc, "//dl/dd/strong[.='Date']/following-sibling::em",
	)
	if err != nil {
		return nil, err
	}
	if em == nil {
		return nil, errors.New("Couldn't find date")
	}
	return em, nil
}

func findSummary(doc *html.Node) (*html.Node, error) {
	dd, err := htmlquery.Query(
		doc, "//dl/dd/strong[.='Date']/../following-sibling::node()",
	)
	if err != nil {
		return nil, err
	}
	if dd == nil {
		return nil, errors.New("Couldn't find summary")
	}
	return dd, nil
}

func constructQuery(id, list string) string {
	subs := msgIdPat.FindStringSubmatch(strings.TrimSpace(id))
	if len(subs) == 2 {
		id = subs[1]
	}
	params := []string{
		"query=" + url.QueryEscape("+message-id:<" + id + ">"),
		"submit=" + url.QueryEscape("Search!"),
		"idxname=" + url.QueryEscape(list),
		"max=1",
		"result=normal",
		"sort=score",
	}
	return namaUrl + "?" + strings.Join(params, "&")
}

// from net/url/example_test
func toJSON(m interface{}) string {
	js, err := json.Marshal(m)
	if err != nil {
		logger.Fatal(err)
	}
	return strings.ReplaceAll(string(js), ",", ", ")
}

func handleNotFound () int {
	if flagJson {
		error := &errorInfo{Code: flagExitCode, Message: "Not found"}
		fmt.Fprint(outputStream, toJSON(&notFoundInfo{Error: error}))
		return 0
	}
	return flagExitCode
}

func handleFound(doc *html.Node) (string, error) {
	firstA, err := findFirstA(doc)
	if err != nil {
		return "", err
	}
	url := baseUrl + htmlquery.SelectAttr(firstA, "href")
	if ! flagJson {
		fmt.Fprint(outputStream, url)
		os.Exit(0)
	}
	date, err := findDate(doc)
	if err != nil {
		return "", err
	}
	summary, err := findSummary(doc)
	if err != nil {
		return "", err
	}
	extra := &extraInfo{
		Url: url,
		Subject: htmlquery.InnerText(firstA),
		Date: htmlquery.InnerText(date),
		Summary: strings.TrimSpace(htmlquery.InnerText(summary)),
	}
	return toJSON(extra), nil
}

func LoadURLOverSOCKS(query string, url *url.URL) (*html.Node, error) {
	dialer, err := proxy.FromURL(url, nil) // &forward net.Dialer
	if err != nil {
		return nil, err
	}
	httpClient := &http.Client{Transport: &http.Transport{Dial: dialer.Dial}}
	resp, err := httpClient.Get(query)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	r, err := charset.NewReader(resp.Body, resp.Header.Get("Content-Type"))
	if err != nil {
		return nil, err
	}
	return html.Parse(r)
}

var exit func(int) = os.Exit

func main() {
	initOpts()
	var doc *html.Node
	var err error
	if flagFile != "" {
		if flagFile == "-" {
			doc, err = html.Parse(os.Stdin)
		} else {
			// Something like "testdata/namazu-no-results.html"
			doc, err = htmlquery.LoadDoc(flagFile)
		}
	} else {
		query := constructQuery(flagMessageId, flagListName)
		if socksAddr := os.Getenv("SOCKS_PROXY"); socksAddr != "" {
			proxyURL, err := url.Parse(socksAddr)
			if err != nil {
				logger.Fatalf("%v", err)
			}
			doc, err = LoadURLOverSOCKS(query, proxyURL)
		} else {
			doc, err = htmlquery.LoadURL(query)
		}
	}
	if err != nil {
		logger.Fatalf("%v", err)
	}
	resCount, err := getResultCount(doc)
	if err != nil {
		logger.Fatalf("%v", err)
	}
	if resCount == 0 {
		exit(handleNotFound())
		return
	}
	result, err := handleFound(doc)
	if err != nil {
		logger.Fatalf("%v", err)
	}
	fmt.Fprint(outputStream, result)
}
