package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
)

func TestConstructQuery(t *testing.T) {
	id := "<jwvwnqb6sfr.fsf-monnier+emacs@gnu.org>"
	list := "help-gnu-emacs"
	res := constructQuery(id, list)
	expect := "https://lists.gnu.org/archive/cgi-bin/namazu.cgi"+
		"?query=%2Bmessage-id%3A%3Cjwvwnqb6sfr.fsf-monnier%2Bemacs%40gnu.org%3E"+
		"&submit=Search%21&idxname=help-gnu-emacs"+
		"&max=1&result=normal&sort=score"
	if res != expect {
		t.Error("Failed")
	}
}

var expected = &extraInfo{
	Url: "https://lists.gnu.org/archive/html/help-gnu-emacs/2021-06/msg00929.html",
	Subject: "Re: [OFFTOPIC] Semver",
	Date: "Wed,  30 Jun 2021 16:00:04 -0400",
	Summary: "Strictly speaking,  any change to the code can break something"+
		" somewhere (https://xkcd.com/1172/),  but the general rule to"+
		" preserve forward compatibility is \"don't introduce new features\"."+
		" You can pr",
}

func TestMain(t *testing.T) {
	flagFile = "testdata/namazu.html"
	flagJson = true
	var b bytes.Buffer
	outputStream = &b
	main()
	var e extraInfo
	if err := json.Unmarshal(b.Bytes(), &e); err != nil {
		t.Error("Failed")
	}
	if e.Url != expected.Url {
		t.Errorf("Failed, %#v", e)
	}
	if e.Subject != expected.Subject {
		t.Errorf("Failed, %#v", e)
	}
	if e.Date != expected.Date {
		t.Errorf("Failed, %#v", e)
	}
	if e.Summary != expected.Summary {
		t.Errorf("Failed, %#v", e)
	}
}

type fakeLogger struct {
	buf []string
}

func (b *fakeLogger) Fatal(v ...interface{}) {
	b.buf = append(b.buf, fmt.Sprint(v...))
}

func (b *fakeLogger) Fatalf(format string, v ...interface{})  {
	b.buf = append(b.buf, (fmt.Sprintf(format, v...)))
}

func TestNoResults(t *testing.T) {
	flagFile = "testdata/namazu-no-results.html"
	flagJson = true
	var b bytes.Buffer
	outputStream = &b
	fakeLogger := fakeLogger{}
	logger = &fakeLogger
	var ec int
	exit = func(i int) { ec = i; }
	main()
	if ec != 0 {
		t.Error("Bad exit code:", ec)
	}
	if len(fakeLogger.buf) != 0 {
		t.Error("Unexpected log")
	}
	if b.String() != "{\"error\":{\"code\":0, \"message\":\"Not found\"}}" {
		t.Error("Bad output")
	}
}

func TestNoResults2022(t *testing.T) {
	flagFile = "testdata/no-results-2022.html"
	flagJson = false
	var b bytes.Buffer
	outputStream = &b
	fakeLogger := fakeLogger{}
	logger = &fakeLogger
	var ec int
	exit = func(i int) { ec = i; }
	flagExitCode = 42
	main()
	if ec != 42 {
		t.Error("Bad exit code:", ec)
	}
	if len(fakeLogger.buf) != 0 {
		t.Error("Unexpected log", fakeLogger.buf)
	}
	if b.String() != "" {
		t.Error("Bad output", b.String())
	}
}
