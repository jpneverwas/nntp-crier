From nobody Fri Aug 13 00:14:09 2021
X-From-Line: nobody Thu Aug 12 03:56:47 2021
From: "J.P." <jp@neverwas.me>
To: Mattias =?utf-8?Q?Engdeg=C3=A5rd?= <mattiase@acm.org>
Cc: 50005@debbugs.gnu.org, Amin Bandali <bandali@gnu.org>
Subject: Re: bug#50005: 28.0.50; silence a couple byte-compiler warnings in ERC
References: <50A0B8E0-2F07-4AAC-8170-27990F9FEAD4@acm.org>
X-Draft-From: ("nnimap+neverwas:INBOX" 369)
Date: Thu, 12 Aug 2021 03:56:45 -0700
In-Reply-To: <50A0B8E0-2F07-4AAC-8170-27990F9FEAD4@acm.org> ("Mattias
	=?utf-8?Q?Engdeg=C3=A5rd=22's?= message of "Thu, 12 Aug 2021 10:15:45
 +0200")
Message-ID: <87a6lngccy.fsf@neverwas.me>
User-Agent: Gnus/5.13 (Gnus v5.13) Emacs/28.0.50 (gnu/linux)
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="=-=-="
Lines: 268
Xref: tnaqb sent.2021-08:18
X-Gnus-Article-Number: 18   Thu, 12 Aug 2021 03:56:47 -0700

--=-=-=
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: quoted-printable

Mattias Engdeg=C3=A5rd <mattiase@acm.org> writes:

> Oops (two typos), but thanks for illustrating the problems with:

Thanks, I spotted the weird double-quote mismatch thing earlier, but I
guess I'm still missing something else? Sorry, I'm quite blind/dumb, as
I'm sure you've gathered.

Anyway, I suppose, if we're using `rx-to-string' or `regexp-opt', then
that entire line can just be 86ed.

>  (rx-to-string `(+ (in ,@(string-to-list erc-lurker-ignore-chars))) t)
>
> or
>
>  (concat (regexp-opt (mapcar #'char-to-string erc-lurker-ignore-chars)) "=
+")

So, I take this to mean there's no problem with requiring (autoloading)
rx.el (or regexp-opt.el), right? I mean, I'd really like to use rx forms
elsewhere in ERC. So if nobody has a problem with it, let's do it! (Cc.
bandali.)

And I guess the `string-to-list' is necessary because

  (rx-to-string '(+ (in "abc")) t)
   =E2=87=92 "[a-c]+"

is still interpreted as a set of character alternatives, whereas

  (rx-to-string '(+ (in ?a ?- ?c)) t)
   =E2=87=92 "[ac-]+"

combines individual character args into just such a set. Thanks for
pointing that out. I'm sure I would have missed that.

Do you use ERC, Mattias? If so, are you available to review more
patches? I'm obviously quite ignorant in the ways of Emacs but am
pushing hard to improve ERC. Some might say that's a recipe for
embarrassment/disaster (though I'm pretty sure I already crossed that
first bridge ages ago).

So what do you say, can you help? I of course would welcome the
experience and obviously don't mind sharing (or even surrendering
complete) credit for anything. (Guess you'd have to weight any potential
cap feathers/plaudits against the indelible smirch of being associated
with the likes of me.) But let me know either way! Thanks.


--=-=-=
Content-Type: text/x-patch
Content-Disposition: attachment;
 filename=0001-Fix-mistake-in-test-for-erc-ring-previous-command.patch

> From 552c40c20249bc3d90463e176a623e00a4532c9a Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Thu, 13 May 2021 02:27:08 -0700
Subject: [PATCH 1/2] Fix mistake in test for erc-ring-previous-command

* test/lisp/erc/erc-tests.el (erc-ring-previous-command):
The variable erc-send-completed-hook was previously set to nil
permanently, which would affect other tests.
---
 test/lisp/erc/erc-tests.el | 11 +++++++----
 1 file changed, 7 insertions(+), 4 deletions(-)

diff --git a/test/lisp/erc/erc-tests.el b/test/lisp/erc/erc-tests.el
index d13397274a..9efcf4a703 100644
--- a/test/lisp/erc/erc-tests.el
+++ b/test/lisp/erc/erc-tests.el
@@ -61,13 +61,16 @@ erc-ring-previous-command
   (with-current-buffer (get-buffer-create "*#fake*")
     (erc-mode)
     (insert "\n\n")
-    (setq erc-input-marker (make-marker) ; these are all local
-          erc-insert-marker (make-marker)
-          erc-send-completed-hook nil)
+    (should-not (local-variable-if-set-p 'erc-send-completed-hook))
+    (set (make-local-variable 'erc-send-completed-hook) nil) ; skip t (globals)
+    (setq erc-input-marker (make-marker)
+          erc-insert-marker (make-marker))
     (set-marker erc-insert-marker (point-max))
     (erc-display-prompt)
     (should (= (point) erc-input-marker))
-    (add-hook 'erc-pre-send-functions #'erc-add-to-input-ring nil t)
+    ;; Just in case erc-ring-mode is already on
+    (setq-local erc-pre-send-functions nil)
+    (add-hook 'erc-pre-send-functions #'erc-add-to-input-ring)
     ;;
     (cl-letf (((symbol-function 'erc-process-input-line)
                (lambda (&rest _)
-- 
2.31.1


--=-=-=
Content-Type: text/x-patch; charset=utf-8
Content-Disposition: attachment;
 filename=0002-Fix-a-couple-byte-compiler-warnings-in-erc.el.patch
Content-Transfer-Encoding: quoted-printable

> From e898612de9e2df42c6a769fa88eb02eaeb8b0b78 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Wed, 21 Jul 2021 00:26:38 -0700
Subject: [PATCH 2/2] Fix a couple byte-compiler warnings in erc.el
MIME-Version: 1.0
Content-Type: text/plain; charset=3DUTF-8
Content-Transfer-Encoding: 8bit

* lisp/erc/erc.el (erc-lurker-maybe-trim): Prevent warning from
showing up in third-party code re library not loaded by autoloading
rx.el when needed.  Also make function do what it claims to. It now
only removes trailing chars typically appended automatically for
uniquifying purposes when a desired nick is already taken. Special
thanks to Mattias Engdeg=C3=A5rd for making this more
respectable. Bug#50005.

(erc-with-all-buffers-of-server): Mute byte compiler warning saying
return value unused. Leave possible optimizations for some future
person.
---
 lisp/erc/erc.el            | 25 ++++--------
 test/lisp/erc/erc-tests.el | 79 ++++++++++++++++++++++++++++++++++++++
 2 files changed, 87 insertions(+), 17 deletions(-)

diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index 73202016ba..ec3ed354b6 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -1732,20 +1732,11 @@ erc-with-all-buffers-of-server
 where PRED matches or in all buffers of the server process if PRED is
 nil."
   (declare (indent 1) (debug (form form body)))
-  ;; Make the evaluation have the correct order
-  (let ((pre (make-symbol "pre"))
-        (pro (make-symbol "pro")))
-    `(let* ((,pro ,process)
-            (,pre ,pred)
-            (res (mapcar (lambda (buffer)
-                           (with-current-buffer buffer
-                             ,@forms))
-                         (erc-buffer-list ,pre
-                                          ,pro))))
-       ;; Silence the byte-compiler by binding the result of mapcar to
-       ;; a variable.
-       (ignore res)
-       res)))
+  (macroexp-let2 nil pred pred
+    `(erc-buffer-filter (lambda ()
+                          (when (or (not ,pred) (funcall ,pred))
+                            ,@forms))
+                        ,process)))
=20
 (define-obsolete-function-alias 'erc-iswitchb #'erc-switch-to-buffer "25.1=
")
 (defun erc--switch-to-buffer (&optional arg)
@@ -2583,9 +2574,9 @@ erc-lurker-maybe-trim
 Returns NICK unmodified unless `erc-lurker-trim-nicks' is
 non-nil."
   (if erc-lurker-trim-nicks
-      (replace-regexp-in-string
-       (regexp-opt-charset (string-to-list erc-lurker-ignore-chars))
-       "" nick)
+      (string-trim-right
+       nick (rx-to-string `(+ (in ,@(string-to-list
+                                     erc-lurker-ignore-chars)))))
     nick))
=20
 (defcustom erc-lurker-hide-list nil
diff --git a/test/lisp/erc/erc-tests.el b/test/lisp/erc/erc-tests.el
index 9efcf4a703..d1a1405999 100644
--- a/test/lisp/erc/erc-tests.el
+++ b/test/lisp/erc/erc-tests.el
@@ -47,6 +47,85 @@ erc--read-time-period
   (cl-letf (((symbol-function 'read-string) (lambda (&rest _) "1d")))
     (should (equal (erc--read-time-period "foo: ") 86400))))
=20
+(ert-deftest erc-with-all-buffers-of-server ()
+  (let (proc-exnet
+        proc-onet
+        erc-kill-channel-hook erc-kill-server-hook erc-kill-buffer-hook)
+
+    (with-current-buffer (get-buffer-create "OtherNet")
+      (erc-mode)
+      (setq proc-onet (start-process "sleep" (current-buffer) "sleep" "1")
+            erc-server-process proc-onet
+            erc-network 'OtherNet)
+      (set-process-query-on-exit-flag erc-server-process nil))
+
+    (with-current-buffer (get-buffer-create "ExampleNet")
+      (erc-mode)
+      (setq proc-exnet (start-process "sleep" (current-buffer) "sleep" "1")
+            erc-server-process proc-exnet
+            erc-network 'ExampleNet)
+      (set-process-query-on-exit-flag erc-server-process nil))
+
+    (with-current-buffer (get-buffer-create "#foo")
+      (erc-mode)
+      (setq erc-server-process proc-exnet)
+      (setq erc-default-recipients '("#foo")))
+
+    (with-current-buffer (get-buffer-create "#spam")
+      (erc-mode)
+      (setq erc-server-process proc-onet)
+      (setq erc-default-recipients '("#spam")))
+
+    (with-current-buffer (get-buffer-create "#bar")
+      (erc-mode)
+      (setq erc-server-process proc-onet)
+      (setq erc-default-recipients '("#bar")))
+
+    (with-current-buffer (get-buffer-create "#baz")
+      (erc-mode)
+      (setq erc-server-process proc-exnet)
+      (setq erc-default-recipients '("#baz")))
+
+    (should (eq (get-buffer-process "ExampleNet") proc-exnet))
+    (erc-with-all-buffers-of-server (get-buffer-process "ExampleNet")
+      nil
+      (kill-buffer))
+
+    (should-not (get-buffer "ExampleNet"))
+    (should-not (get-buffer "#foo"))
+    (should-not (get-buffer "#baz"))
+    (should (get-buffer "OtherNet"))
+    (should (get-buffer "#bar"))
+    (should (get-buffer "#spam"))
+
+    (let* ((test (lambda () (not (string=3D (buffer-name) "#spam"))))
+           (calls 0)
+           (get-test (lambda () (cl-incf calls) test)))
+
+      (erc-with-all-buffers-of-server proc-onet
+        (funcall get-test)
+        (kill-buffer))
+
+      (should (=3D calls 1)))
+
+    (should-not (get-buffer "OtherNet"))
+    (should-not (get-buffer "#bar"))
+    (should (get-buffer "#spam"))
+    (kill-buffer "#spam")))
+
+(ert-deftest erc-lurker-maybe-trim ()
+  (let (erc-lurker-trim-nicks
+        (erc-lurker-ignore-chars "_`"))
+
+    (should (string=3D "nick`" (erc-lurker-maybe-trim "nick`")))
+
+    (setq erc-lurker-trim-nicks t)
+    (should (string=3D "nick" (erc-lurker-maybe-trim "nick`")))
+    (should (string=3D "ni`_ck" (erc-lurker-maybe-trim "ni`_ck__``")))
+
+    (setq erc-lurker-ignore-chars "_-`") ; set of chars, not character alts
+    (should (string=3D "nick" (erc-lurker-maybe-trim "nick-_`")))))
+
 (ert-deftest erc-ring-previous-command-base-case ()
   (ert-info ("Create ring when nonexistent and do nothing")
     (let (erc-input-ring
--=20
2.31.1


--=-=-=--

