package main

import (
	"flag"
	"io"
	"log"
	"net/mail"
	"os"

	"gitlab.com/jpneverwas/nntp-crier/pkg/briefen"
	. "gitlab.com/jpneverwas/nntp-crier/pkg/compose"
	"gitlab.com/jpneverwas/nntp-crier/pkg/filter"
	"gitlab.com/jpneverwas/nntp-crier/pkg/types"
)

var logger = log.Default()

var flagSource string
var flagUrl string
var flagMax int

func main() {
	flag.IntVar(&flagMax, "max", -1, "Max chars; -1 means don't cap")
	flag.StringVar(&flagSource, "source", "", "Message to load")
	flag.StringVar(&flagUrl, "url", "", "Mailman URL")
	flag.Parse()
	var file io.Reader
	var err error
	if flagSource == "" {
		flag.Usage()
		os.Exit(1)
	}
	if flagSource == "-" {
		file = os.Stdin
	} else {
		file, err = os.Open(flagSource)
		if err != nil {
			logger.Fatalf("%v", err)
		}
	}
	msg, err := mail.ReadMessage(file)
	if err != nil {
		logger.Fatalf("%v", err)
	}
	briefen.ContentHandlers = make(map[string]types.MediaHandlerFactory)
	briefen.ContentHandlers["text/plain"] = briefen.CreateTextPlain
	briefen.ContentFilters = make(map[string][]types.MediaFilter)
	briefen.ContentFilters["text/plain"] = []types.MediaFilter{
		FilterRMS, FilterInitialedQuotes, FilterInsertMarks,
	}
	briefen.ContentHandlers["multipart/alternative"] = briefen.CreateMultiAlt
	filter.MessageFilterVia(msg.Header)
	if err = Render(os.Stdout, msg, BasicTemplate, flagUrl, flagMax); err != nil {
		logger.Fatalf("%v", err)
	}
}
