package main

import (
	"regexp"
	"strings"

	"gitlab.com/jpneverwas/nntp-crier/pkg/briefen"
	"gitlab.com/jpneverwas/nntp-crier/pkg/types"
)

func isRMS(h types.Header) bool {
	from, err := h.AddressList("from")
	if err != nil {
		panic(err)
	}
	addr := from[0]
	return addr.Name == "Richard Stallman" || addr.Address == "rms.gnu.org"
}

// FilterRMS removes the (in)famous privacy header from RMS authored emails.
func FilterRMS(mh types.MediaHandler, line []byte) []byte {
	tp, ok := mh.(*briefen.MHTextPlain)
	if !ok {
		panic("FilterRMS requires MHTextPlain")
	}
	if tp.GetLineNumber() > 3 {
		return line
	}
	if !isRMS(tp.GetHeader()) {
		return line
	}
	s := string(line)
	if strings.HasPrefix(s, "[[[ ") && strings.HasSuffix(s, " ]]]") {
		return []byte{}
	}
	return line
}

func createInitials(name string) string {
	parts := strings.Split(name, " ")
	out := []rune{}
	for _, part := range parts {
		if part != "" {
			out = append(out, []rune(part)[0])
		}
	}
	return string(out)
}

func getInitials(tp *briefen.MHTextPlain) []string {
	storage := tp.GetStorage()
	existing, ok := storage["initialedQuotes"]
	if ok {
		if existing, ok := existing.([]string); ok {
			return existing
		}
	}
	out := make([]string, 0)
	for _, addr := range tp.GetAddressees() {
		if addr.Name == "" {
			continue
		}
		out = append(out, createInitials(addr.Name))
	}
	storage["initialedQuotes"] = out
	return out
}

var iqPattern = regexp.MustCompile("^([[:alpha:]]{1,3})> ")

func cacheInitials(tp *briefen.MHTextPlain, s string) {
	storage := tp.GetStorage()
	existing, ok := storage["initialedQuotes"]
	if !ok {
		storage["initialedQuotes"] = make([]string, 0)
		existing = storage["initialedQuotes"]
	}
	if existing, ok := existing.([]string); ok {
		storage["initialedQuotes"] = append(existing, s)
		return
	}
	panic("Bad storage type")
}

// FilterInitialedQuotes removes lines with initials-prefixed quote markup
func FilterInitialedQuotes(mh types.MediaHandler, line []byte) []byte {
	tp, ok := mh.(*briefen.MHTextPlain)
	if !ok {
		panic("FilterInitialedQuotes requires MHTextPlain")
	}
	s := string(line)
	// This person remembered to CC everyone
	for _, initials := range getInitials(tp) {
		if strings.HasPrefix(s, initials+">") {
			return []byte{}
		}
	}
	// Sender isn't a Cc'er
	if subs := iqPattern.FindStringSubmatch(s); len(subs) != 0 {
		cacheInitials(tp, subs[1])
		return []byte{}
	}
	return line
}

var insertMarkersPat = regexp.MustCompile(
	`^[-]+\d<[-]+[\w ]+[-]+[\w ]+[-]+>\d[-]+`,
)

// FilterInsertMarks removes section delims around inserted preformatted text.
// Emacs users see option message-mark-insert-begin in lisp/gnus/message.el.
func FilterInsertMarks(mh types.MediaHandler, line []byte) []byte {
	if insertMarkersPat.Match(line) {
		return nil
	}
	return line
}
