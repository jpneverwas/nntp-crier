package main

import (
	"testing"
)

func TestCreateInitials(t *testing.T) {
	if createInitials("") != "" {
		t.Error("Unexpected")
	}
	if createInitials("J. Random Hacker") != "JRH" {
		t.Error("Unexpected")
	}
	if createInitials("Bob") != "B" {
		t.Error("Unexpected")
	}
	if createInitials("student") != "s" {
		t.Error("Unexpected")
	}
	if createInitials("白鵬翔") != "白" {
		t.Error("Unexpected")
	}
}
