package main

import (
	"testing"

	helpers "gitlab.com/jpneverwas/nntp-crier/internal/testing"
)

func TestTo(t *testing.T) {
	helpers.RunAsChild(
		t, "To", "testdata/to.email", "testdata/to.expect", main,
	)
}

func TestFrom(t *testing.T) {
	helpers.RunAsChild(
		t, "From", "testdata/from.email", "testdata/from.expect", main,
	)
}

func TestRMS(t *testing.T) {
	helpers.RunAsChild(
		t, "RMS", "testdata/rms.email", "testdata/rms.expect", main,
	)
}

func TestInitialedQuotes(t *testing.T) {
	helpers.RunAsChild(
		t, "InitialedQuotes", "testdata/iq.email", "testdata/iq.expect", main,
	)
}

func TestVia(t *testing.T) {
	helpers.RunAsChild(
		t, "Via", "testdata/via.email", "testdata/via.expect", main,
	)
}

func TestMultAlt(t *testing.T) {
	helpers.RunAsChild(
		t, "MultAlt", "testdata/multalt.email", "testdata/multalt.expect", main,
	)
}

func TestSplitWrites(t *testing.T) {
	helpers.RunAsChild(
		t, "MultAlt", "testdata/splitwrites.email", "testdata/splitwrites.expect", main,
	)
}

func TestBase64(t *testing.T) {
	helpers.RunAsChild(
		t, "MultAlt", "testdata/base64.email", "testdata/base64.expect", main,
	)
}

func TestInsertMarks(t *testing.T) {
	helpers.RunAsChild(
		t, "InsertMarks", "testdata/insertmarks.email", "testdata/insertmarks.expect", main,
	)
}

func TestMultiBut(t *testing.T) {
	helpers.RunAsChild(
		t, "MultiBut", "testdata/multibut.email", "testdata/multibut.expect", main,
	)
}

func TestSubject(t *testing.T) {
	helpers.RunAsChild(
		t, "Subject", "testdata/subject.email", "testdata/subject.expect", main,
	)
}
