package main

import (
	"gitlab.com/jpneverwas/nntp-crier/pkg/filter"
	"gitlab.com/jpneverwas/nntp-crier/internal/crud"
)

func filterGmaneCrossPost(s string) string {
	return filter.GmanePat.ReplaceAllString(s, "")
}

var rememberedFilters = map[string]func(string)string{
	"gmaneCrossPost": filterGmaneCrossPost,
}


// checkRemembered says whether an article has already been cried out.
//
// This normalizes IDs by applying a set of rememberedFilters before checking.
func (session session) checkRemembered(messageId string) (bool, error) {
	// TODO replace this by after itering over config list
	for _, f := range rememberedFilters {
		messageId = f(messageId)
	}
	return crud.Has(session.store, messageId)
}

// rememberId adds a new entry in the database with the Message ID as the key.
// The value is the group name, but it's currently unused.
func (session session) rememberId(id, group string) error {
	return crud.SetOne(session.store, id, group)
}
