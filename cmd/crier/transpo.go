package main

import (
	"crypto/tls"
	"net"
	"net/url"
	"os"
	"strings"

	nntpclient "github.com/dustin/go-nntp/client"
	"golang.org/x/net/proxy"
)

func hasTLSport(server string) bool {
	return strings.HasSuffix(server, ":563")
}

func CreateSimpleTLSConfig(serverName string) (*tls.Config, error) {
	host, _, err := net.SplitHostPort(serverName)
	if err != nil {
		return nil, err
	}
	return &tls.Config{ServerName: host}, nil
}

// Internal option for testing
var insecureSkipVerify = os.Getenv("CRIER_TLS_INSECURESKIPVERIFY") != ""

func newSocks(server string, url *url.URL) (*nntpclient.Client, error) {
	dialer, err := proxy.FromURL(url, nil) // &forward net.Dialer
	if err != nil {
		return nil, err
	}
	conn, err := dialer.Dial("tcp", server)
	if err != nil {
		return nil, err
	}
	if hasTLSport(server) {
		config, err := CreateSimpleTLSConfig(server)
		if err != nil {
			return nil, err
		}
		if insecureSkipVerify {
			config.InsecureSkipVerify = true
		}
		conn = tls.Client(conn, config)
	}
	return nntpclient.NewConn(conn)
}

func (session session) connect() (*nntpclient.Client, error) {
	// Something like: 127.0.0.1:9050
	if socksAddr := session.proxy; socksAddr != "" {
		if ! strings.HasPrefix(socksAddr, "socks") {
			socksAddr = "socks5h://" + socksAddr;
		}
		proxyURL, _ := url.Parse(socksAddr)
		return newSocks(session.server, proxyURL)
	}
	if hasTLSport(session.server) {
		config, err := CreateSimpleTLSConfig(session.server)
		if err != nil {
			return nil, err
		}
		if insecureSkipVerify {
			config.InsecureSkipVerify = true
		}
		return nntpclient.NewTLS("tcp", session.server, config)
	}
	return nntpclient.New("tcp", session.server)
}
