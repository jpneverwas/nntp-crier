package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"
	"testing"
	"time"

	nntpclient "github.com/dustin/go-nntp/client"
	"github.com/tidwall/buntdb"
	"gitlab.com/jpneverwas/nntp-crier/internal/crud"
	"golang.org/x/tools/go/packages"
)

func findNntpExampleServer () (string, error) {
	pkgs, err := packages.Load(nil, "github.com/dustin/go-nntp")
	if err != nil {
		return "", err
	}
	if len(pkgs) != 1 {
		return "", fmt.Errorf("Couldn't find root")
	}
	files := pkgs[0].GoFiles
	root := filepath.Dir(files[0])
	found := filepath.Join(root, "examples/server/exampleserver.go")
	if _, err := os.Stat(found); err != nil {
		return "", err
	}
	return found, nil
}

func testStartServer(t *testing.T, tmpdir string) func() {
	srvLogPath := filepath.Join(tmpdir, "srv.out")
	srvLogFile, err := os.Create(srvLogPath)
	if err != nil {
		t.Error(err)
	}
	path, err := exec.LookPath("go")
	if err != nil {
		t.Error(err)
	}
	target, err := findNntpExampleServer()
	if err != nil {
		t.Error(err)
	}
	cmd := exec.Command(path, "run", target)
	cmd.Stderr = srvLogFile
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	err = cmd.Start()
	if err != nil {
		t.Error(err)
	}

	return func() {
		pgid, err := syscall.Getpgid(cmd.Process.Pid)
		if err == nil {
			syscall.Kill(-pgid, 15)  // note the minus sign
		}

		cmd.Wait()
	}
}

const examplehandler = `#!/bin/sh
echo ARGS START
printf '\t%s\n' "$@"
echo ARGS END
echo INPUT START
cat
echo INPUT END
`

func uploadArts(group string, queue []string) error {
	c, err := nntpclient.New("tcp", "localhost:1119")
	if err != nil {
		return err
	}
	defer c.Close()
	if _, _, err = c.Command("mode reader", 2); err != nil {
		return err
	}
	if _, err := c.Group(group); err != nil {
		return err
	}
	for _, post := range queue {
		err = c.Post(strings.NewReader(post))
		// Sleep no matter what
		time.Sleep(100 * time.Millisecond)
		if err != nil {
			return err
		}
	}
	time.Sleep(time.Second)
	return nil
}

func compareResult(a, b string) error {
	expect, err := ioutil.ReadFile(a)
	if err != nil {
		return err
	}
	result, err := ioutil.ReadFile(b)
	if err != nil {
		return err
	}
	expect = bytes.TrimSpace(expect)
	result = bytes.TrimSpace(result)
	rv := bytes.Compare(expect, result)
	if rv != 0 {
		for i := 0; ; i++ {
			if i == len(expect) {
				fmt.Printf("Got more lines than expected")
				break
			}
			if i == len(result) {
				fmt.Printf("Got fewer lines than expected")
				break
			}
			if result[i] != expect[i] {
				fmt.Printf(
					"@char # %d\nexpect < %02x:'%c'\nresult > %02x:'%c'\n",
					i, expect[i], expect[i], result[i], result[i],
				)
				break
			}
		}
		return fmt.Errorf("mismatch %d:\n\n%s\n=====\n%s", rv, expect, result)
	}
	return nil
}

func inspectLines(
	sentinelPattern, logPath string, r io.Reader, t *testing.T, done chan int,
) {
	scanner := bufio.NewScanner(r)
	logFile, err := os.Create(logPath)
	var lineCt int
	if err != nil {
		t.Errorf("%v", err)
	}
	var text string
	for scanner.Scan() {
		text = scanner.Text()
		fmt.Println(scanner.Text())
		logFile.Write(scanner.Bytes())
		logFile.Write([]byte{'\n'})
		lineCt++
		m, err := regexp.MatchString("SHOULD_NOT_APPEAR", text)
		if err != nil {
			t.Errorf("%v", err)
		}
		if m {
			t.Error("Bad match found")
		}
		m, err = regexp.MatchString(sentinelPattern, text)
		if err != nil {
			t.Errorf("%v", err)
		}
		if m {
			fmt.Println("Found done sentinel, quitting.")
			done <- lineCt
		}
	}
}

var part1 = []string{
	`From: <nobody@example.com>
Message-Id: <0000000000000000000001.one@example.com>
Newsgroups: misc.test
Subject: First test
Organization: First Example Com

This SHOULD_NOT_APPEAR because on the first encounter with any group,
the group's highest is recorded as the last seen. So this one just
gets skipped over.
`,
	`From: <mystifying_einstein@infalliblewilliams.org>
Message-Id: <000000000000000002.two@infalliblewilliams.org>
Newsgroups: misc.test
Subject: Foo is me
Organization: Infalliblewilliams Org

Another test post. SHOULD_NOT_APPEAR This one shouldn't match.
`,
}

var crierTestTempDir string

func initialRun(t *testing.T) {
	fmt.Println("Uploading 2 articles")
	if err := uploadArts("misc.test", part1); err != nil {
		t.Errorf("%v", err)
	}

	patternArgs = []string{"misc.test=test"}
	fmt.Println("Running in 2")
	time.Sleep(2*time.Second)
	r, w  := io.Pipe()
	logger = log.New(w, "", 0)

	logPath := filepath.Join(crierTestTempDir, t.Name() + ".log")
	go main()

	done := make(chan int)
	go inspectLines("key: misc\\.test, value: 2", logPath, r, t, done)
	<-done
	if err := compareResult("testdata/01_expect.log", logPath); err != nil {
		t.Errorf("%v", err)
	}
}

var partx = []string{
	`From: <sharp@dijkstra.com>
Message-Id: <0000000000000000000042.randomone@sharp@dijkstra.com>
Newsgroups: alt.test
Subject: Hi random
Organization: First nonsense

This SHOULD_NOT_APPEAR just filler
`,
}

var part2 = []string{
	`From: <adoring@lamport.org>
Message-Id: <000000000000000003.three@lamport.org>
Newsgroups: misc.test
Newsgroups: alt.test
Subject: A real test
Organization: Lamps Org

Finally an actual match
`,
}

func firstMatch(t *testing.T) {
	fmt.Println("Uploading 1 article in each group")
	if err := uploadArts("alt.test", partx); err != nil {
		t.Errorf("%v", err)
	}
	if err := uploadArts("misc.test", part2); err != nil {
		t.Errorf("%v", err)
	}

	patternArgs = []string{"misc.test=test"}
	fmt.Println("Running in 2")
	time.Sleep(2*time.Second)
	r, w  := io.Pipe()
	logger = log.New(w, "", 0)

	logPath := filepath.Join(crierTestTempDir, t.Name() + ".log")
	go main()

	done := make(chan int)
	go inspectLines("key: misc\\.test, value: 3", logPath, r, t, done)
	<-done
	if err := compareResult("testdata/02_expect.log", logPath); err != nil {
		t.Errorf("%v", err)
	}
}

func dupeSkipped(t *testing.T) {
	patternArgs = []string{"alt.test=test"}
	fmt.Println("Running in 2")
	time.Sleep(2*time.Second)
	r, w  := io.Pipe()
	logger = log.New(w, "", 0)

	logPath := filepath.Join(crierTestTempDir, t.Name() + ".log")
	go main()

	done := make(chan int)
	go inspectLines("key: misc\\.test, value: 3", logPath, r, t, done)
	<-done
	if err := compareResult("testdata/03_expect.log", logPath); err != nil {
		t.Errorf("%v", err)
	}
}

var part4 = []string{
	`From: <happy@fermat.org>
Message-Id: <000000000000000005.happy@fermat.org>
References: <83b8oazdlb.sfs@tah.org>
 <000000000000000003.three@lamport.org>
 <874xqoue8o.sfs@tznvy.com>
Newsgroups: misc.test
Subject: Adoring fan
Organization: Fermat Org

This must match even though the subject line doesn't contain 'test'
`,
}

func indirectMatch(t *testing.T) {
	fmt.Println("Uploading 1 article in each group")
	if err := uploadArts("misc.test", part4); err != nil {
		t.Errorf("%v", err)
	}

	patternArgs = []string{"misc.test=test"}
	fmt.Println("Running in 2")
	time.Sleep(2*time.Second)
	r, w  := io.Pipe()
	logger = log.New(w, "", 0)

	logPath := filepath.Join(crierTestTempDir, t.Name() + ".log")
	go main()

	done := make(chan int)
	go inspectLines("key: misc\\.test, value: 4", logPath, r, t, done)
	<-done
	if err := compareResult("testdata/04_expect.log", logPath); err != nil {
		t.Errorf("%v", err)
	}
}

var part5 = []string{
	`From: <thirsty@blackwell.org>
Message-Id: <000000000000000006.thirsty@blackwell.org>
References: <fakefakefake.sfs@afakebfakec.org>
Newsgroups: misc.test
Subject: Some test
Organization: Blackwell Org

This should match
`, `From: <quirky@mestorf.org>
Message-Id: <000000000000000???.quirky@mestorf.org>
References: <wut.sfs@wild888.org>
Newsgroups: misc.test
Subject: Section 6.3
Organization: Mestorf Org

This SHOULD_NOT_APPEAR just filler again
`,
}

func handlerDefer(t *testing.T) {
	// Add one that expires in 2030
	getExpiration = func(_ time.Duration) time.Time {
		return time.Unix(1893456000, 0).UTC()
	}
	handlerPath := filepath.Join(crierTestTempDir, "handler-ignore.sh")
	ioutil.WriteFile(handlerPath, []byte(examplehandler + "\nexit 42\n"), 0700)
	if err := os.Setenv("CRIER_HANDLER", handlerPath); err != nil {
		t.Errorf("%v", err)
	}

	fmt.Println("Uploading 2 articles")
	if err := uploadArts("misc.test", part5); err != nil {
		t.Errorf("%v", err)
	}

	patternArgs = []string{"misc.test=test"}
	fmt.Println("Running in 2")
	time.Sleep(2*time.Second)
	r, w  := io.Pipe()
	logger = log.New(w, "", 0)

	logPath := filepath.Join(crierTestTempDir, t.Name() + ".log")
	go main()

	done := make(chan int)
	go inspectLines("key: misc\\.test:deferred", logPath, r, t, done)
	<-done
	if err := compareResult("testdata/05_expect.log", logPath); err != nil {
		t.Errorf("%v", err)
	}
}

var part6 = []string{
	`From: <hungry@wozniak.com>
Message-Id: <00000000000000000000ff.exfconts@hungry@wozniak.com>
Newsgroups: misc.test
Subject: Block following numerical
Organization: Hung W

This SHOULD_NOT_APPEAR ever
`,
}

// This one shows that deferred article is requested (deferred again)
func deferredBacklog(t *testing.T) {
	// Entry already exists for article #5, so this is never called
	getExpiration = func(_ time.Duration) time.Time {
		// return time.Unix(0, 0).UTC()
		panic("not called")
	}
	// Restore handler
	handlerPath := filepath.Join(crierTestTempDir, "handler-ignore.sh")
	ioutil.WriteFile(handlerPath, []byte(examplehandler), 0700)
	if err := os.Setenv("CRIER_HANDLER", handlerPath); err != nil {
		t.Errorf("%v", err)
	}

	fmt.Println("Uploading 1 article")
	if err := uploadArts("misc.test", part6); err != nil {
		t.Errorf("%v", err)
	}

	patternArgs = []string{"misc.test=test"}
	fmt.Println("Running in 2")
	time.Sleep(2*time.Second)
	r, w  := io.Pipe()
	logger = log.New(w, "", 0)

	logPath := filepath.Join(crierTestTempDir, t.Name() + ".log")
	go main()

	done := make(chan int)
	go inspectLines("key: misc\\.test:deferred", logPath, r, t, done)
	<-done
	if err := compareResult("testdata/06_expect.log", logPath); err != nil {
		t.Errorf("%v", err)
	}
}

var part7 = []string{
	`From: <blissful@keller.com>
Message-Id: <00000000000000000000fa.123@blissful@keller.com>
Newsgroups: misc.test
Subject: Qualified identifiers numerical
Organization: B K

This SHOULD_NOT_APPEAR ever
`,
}

// This one shows that deferred article is finally expired
func expiredArt(t *testing.T) {
	// Hack database
	db, err := buntdb.Open(os.Getenv("CRIER_DATABASE"))
	if err != nil {
		t.Errorf("%v", err)
	}
	expireHandler := func(d crud.Deferred, k string, exp time.Time) error {
		d[k] = time.Unix(0, 0).UTC()
		return nil
	}
	err = crud.IterDeferred(db, "misc.test", expireHandler)
	if err != nil {
		t.Errorf("%v", err)
	}
	if err = db.Close(); err != nil {
		t.Errorf("%v", err)
	}

	fmt.Println("Uploading 1 article")
	if err := uploadArts("misc.test", part7); err != nil {
		t.Errorf("%v", err)
	}

	patternArgs = []string{"misc.test=test"}
	fmt.Println("Running in 2")
	time.Sleep(2*time.Second)
	r, w  := io.Pipe()
	logger = log.New(w, "", 0)

	logPath := filepath.Join(crierTestTempDir, t.Name() + ".log")
	go main()

	done := make(chan int)
	go inspectLines("key: misc\\.test:deferred", logPath, r, t, done)
	<-done
	if err := compareResult("testdata/07_expect.log", logPath); err != nil {
		t.Errorf("%v", err)
	}
}

func TestAll(t *testing.T) {
	crierTestTempDir = t.TempDir()
	dir := crierTestTempDir

	// Subtests use t.Name() for logs, which include common /TestAll/ parent
	if err := os.Mkdir(filepath.Join(dir, t.Name()), 0775); err != nil {
		t.Errorf("%v", err)
	}
	fmt.Println("Starting server")
	cmdFinalizer := testStartServer(t, dir)
	time.Sleep(2 * time.Second)

	dataPath := filepath.Join(dir, "data.db")
	if err := os.Setenv("CRIER_DATABASE", dataPath); err != nil {
		t.Errorf("%v", err)
	}
	if err := os.Setenv("CRIER_SERVER", "localhost:1119"); err != nil {
		t.Errorf("%v", err)
	}

	handlerPath := filepath.Join(dir, "handler.sh")
	ioutil.WriteFile(handlerPath, []byte(examplehandler), 0700)
	if err := os.Setenv("CRIER_HANDLER", handlerPath); err != nil {
		t.Errorf("%v", err)
	}

	t.Run("Init", initialRun)
	t.Run("firstMatch", firstMatch)
	t.Run("dupeSkipped", dupeSkipped)
	t.Run("indirectMatch", indirectMatch)
	t.Run("handlerDefer", handlerDefer)
	t.Run("deferredBacklog", deferredBacklog)
	t.Run("expiredArt", expiredArt)

	cmdFinalizer()
}
