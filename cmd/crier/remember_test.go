package main

import "testing"


func TestFilterGmaneCrossPost(t *testing.T) {
	// Baseline
	want := "87ilyg5exd.fsf@gnu.org"
	if got := filterGmaneCrossPost(want); got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}

	// Hit
	in := "87ilyg5exd.fsf__24540.8859964058$1633142359$gmane$org@gnu.org"
	if got := filterGmaneCrossPost(in); got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}

	// With brackets
	want = "<87mtns5f4z.fsf@gnu.org>"
	in = "<87mtns5f4z.fsf__10616.3820646302$1633142152$gmane$org@gnu.org>"
	if got := filterGmaneCrossPost(in); got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}

	want = "<87ilq3r5d9.fsf@neverwas.me>"
	in = "<87ilq3r5d9.fsf__45053.505238478$1652837716$gmane$org@neverwas.me>"
	if got := filterGmaneCrossPost(in); got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}
}
