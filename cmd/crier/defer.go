package main

import (
	"regexp"
	"time"

	"gitlab.com/jpneverwas/nntp-crier/internal/crud"
)

func createDeferTime(after time.Duration) time.Time {
	return time.Now().Add(after)
}

func (session session) maybeDeleteDeferred(grp, art string) error {
	// If a deferred entry exists for this article, delete it
	v, err := crud.GetDeferredItem(session.store, grp, art)
	if err != nil {
		return err
	}
	if v == nil {
		return nil
	}
	if err := crud.DeleteDeferredItem(session.store, grp, art); err != nil {
		return err
	}
	return nil
}

func (session session) createDeferredHandler(
	group string, pattern *regexp.Regexp,
) crud.ItemHandler {
	return func (deferred crud.Deferred, art string, expires time.Time) error {
		if expires.Before(time.Now()) {
			logger.Printf("Giving up on expired article: %s", art)
			delete(deferred, art)
			return nil
		}
		logger.Printf("Requesting deferred article: %s", art)
		// We can't just request the art number alone because our test server
		// currently only works with ranges
		if err := session.onceOver(group, pattern, art+"-"+art); err != nil {
			return err
		}
		return nil
	}
}
