package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"math"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"time"

	"strings"

	"gitlab.com/jpneverwas/nntp-crier/internal/crud"

	"github.com/dustin/go-nntp"
	nntpclient "github.com/dustin/go-nntp/client"
	"github.com/tidwall/buntdb"
)

var logger = log.Default() // so we can use custom one in test

var getExpiration func(time.Duration) time.Time = createDeferTime

type session struct {
	server string // TCP host:port combo
	proxy string
	user string
	pass string
	handler string
	period time.Duration
	deferPeriod time.Duration
	fields []string
	store *buntdb.DB
	patterns map[string]*regexp.Regexp
	refless []string
	client *nntpclient.Client
}

// newSession creates a new sessio object and initializes the db.
// Perhaps "session" isn't the best term here since this apply to
// multiple connections.
func newSession() (*session, error) {
	session := &session{
		server: os.Getenv("CRIER_SERVER"),
		proxy: os.Getenv("SOCKS_PROXY"),
		user: os.Getenv("CRIER_USER"),
		pass: os.Getenv("CRIER_PASS"),
		handler: os.Getenv("CRIER_HANDLER"),
		patterns: make(map[string]*regexp.Regexp),
		refless: strings.Split(os.Getenv("CRIER_REFLESS"), ","),
	}
	ecHandlers = make(map[int]ecHandler)
	ecHandlers[0] = ecOK
	if err := session.setECHandler("CRIER_EC_DEFER", 42, ecDefer);
	err != nil {
		return nil, err
	}
	if err := session.setECHandler("CRIER_EC_PUNT", 31, ecPunt);
	err != nil {
		return nil, err
	}

	if deferPeriod := os.Getenv("CRIER_DEFER_PERIOD"); deferPeriod != "" {
		dPeriod, err := time.ParseDuration(deferPeriod)
		if err != nil {
			return nil, err
		}
		session.deferPeriod = dPeriod
	} else {
		session.deferPeriod = time.Hour * 6
	}

	if periodStr := os.Getenv("CRIER_PERIOD"); periodStr != "" {
		period, err := time.ParseDuration(periodStr)
		if err != nil {
			return nil, err
		}
		session.period = period
	}

	if err := session.validateSession(); err != nil {
		logger.Fatalf("Error validating session: %v", err)
	}

	file := "data.db"
	if existing := os.Getenv("CRIER_DATABASE"); existing != "" {
		file = existing
	}
	db, err := buntdb.Open(file)
	if err != nil {
		return nil, err
	}
	session.store = db
	return session, nil
}

var patternArgs []string

// validateSession ensures environment variables and command line args are
// acceptable
func (session session) validateSession() error {
	if len(patternArgs) == 0 {
		patternArgs = os.Args[1:]
	}
	if len(patternArgs) == 0 {
		return fmt.Errorf("No patterns provided")
	}
	if strings.HasPrefix(patternArgs[0], "-") {
		fmt.Println("See README")
		os.Exit(0)
	}
	if session.server == "" {
		return errors.New("Required Env Var CRIER_SERVER unset")
	}
	if session.proxy != "" && ! strings.HasPrefix(session.proxy, "socks") {
		return errors.New("SOCKS_SERVER not pseudo URL like socks5h://")
	}
	for _, arg := range patternArgs {
		spec := strings.SplitN(arg, "=", 2)
		if len(spec) != 2 {
			return fmt.Errorf("Bad arg: %v", spec)
		}
		group, pattern := spec[0], spec[1]
		if group == "" || pattern == "" {
			return fmt.Errorf("Bad arg: %v", arg)
		}
		logger.Printf("Registering group %s with pattern %s", group, pattern)
		session.patterns[group] = regexp.MustCompile(pattern)
	}
	if session.handler == "" {
		return errors.New("Required Env var CRIER_HANDLER missing")
	}
	return nil
}

func (session session) maybeAuth() error {
	// Authenticate
	if session.user == "" || session.pass == "" {
		return nil
	}
	msg, err := session.client.Authenticate(session.user, session.pass)
	if err != nil {
		return err
	}
	logger.Printf("Post authentication message:  %v", msg)
	return nil
}

// maybeGet returns the last article number, if found, otherwise 0
func maybeGet(db *buntdb.DB, name string) (int64, error) {
	lastS, err := crud.Get(db, name)
	if err != nil && err.Error() != "not found" {
		return 0, err
	}
	if err != nil {
		return 0, nil
	}
	return strconv.ParseInt(lastS, 10, 64)
}

// getLast queries the database for the article number of the last
// successfully handled message for group
func (session session) getLast(group *nntp.Group) (int64, error) {
	last, err := maybeGet(session.store, group.Name)
	if err != nil {
		return 0, err
	}
	if last == 0 {
		last = int64(math.Max(float64(group.High - 1), float64(group.Low)))
	}
	logger.Printf("Last seen for %s: %d\n", group.Name, last)
	return last, nil
}

// maybeAddLast records the current article only when larger than latest.
//
// It also removes the deferred status of an article when applicable.
func (session session) maybeAddLast(groupName, artNumStr string) error {
	last, err := maybeGet(session.store, groupName)
	if err != nil {
		return err
	}
	candidate, err := strconv.ParseInt(artNumStr, 10, 64)
	if err != nil {
		return err
	}
	if candidate > last {
		return crud.SetOne(session.store, groupName, artNumStr)
	}
	if err := session.maybeDeleteDeferred(groupName, artNumStr); err != nil {
		return err
	}
	return nil
}

// addLast records the current article number as being the group's last seen.
func (session session) addLast(groupName, artNumStr string) error {
	return crud.SetOne(session.store, groupName, artNumStr)
}

var standardFields = [...]string{
	"Subject:", "From:", "Date:", "Message-Id:", "References:",
	":bytes", ":lines",
}

var maxWait = 2 * time.Minute

func (session session) processArticle (
	articleNumber, group string, parts []string,
) error {
	id := parts[3]
	if seen, err := session.checkRemembered(id); err != nil {
		return err
	} else if seen {
		logger.Printf("Already seen %s for %s", id, group)
		return session.maybeAddLast(group, articleNumber)
	}

	args := make([]string, 8)
	for i, value := range parts {
		if i < len(standardFields) {
			args[i] = fmt.Sprintf("%s %s",  standardFields[i], value)
		} else if session.fields != nil && i < len(session.fields) {
			args[i] = fmt.Sprintf("%s %s",  session.fields[i], value)
		} else {
			logger.Printf("Unknown field for value: %s", value)
			args[i] = fmt.Sprintf("Unknown: %s", value)
		}
	}

	n, idPlus, r, err := session.client.Article(articleNumber)
	if err != nil {
		return err
	}
	if ! strings.HasPrefix(idPlus, id) {
		return fmt.Errorf("Unexpected response: id: %s idPlus: %s", id, idPlus)
	}
	logger.Printf("Response ARTICLE: %d, %#v", n, idPlus)
	rest := append([]string{group}, args...)
	ctx, cancel := context.WithTimeout(context.Background(), maxWait)
	defer cancel()
	cmd := exec.CommandContext(ctx, session.handler, rest...)
	cmd.Stdin = r

	if output, err := cmd.Output(); err != nil {
		if maybeExitError, ok := err.(*exec.ExitError); ok {
			ec := maybeExitError.ProcessState.ExitCode()
			if f, ok := ecHandlers[ec]; ok {
				return f(session, maybeExitError, articleNumber, group, id)
			}
			logger.Printf("Bad exit: ec: %d, err: %s, output: %s",
				ec, maybeExitError.Error(), string(maybeExitError.Stderr))
		}
		return err
	} else {
		logger.Println("Handler succeeded")
		if len(output) != 0 {
			fmt.Printf("Handler output:\n%s", string(output))
		}
	}
	return ecOK(session, nil, articleNumber, group, id)
}

func (session session) seenAny(references... string) (bool, error) {
	return crud.HasAny(session.store, references...)
}

func (session session) isRefless(group string) bool {
	for _, g := range session.refless {
		if g == group {
			return true
		}
	}
	return false
}

const floodStop = time.Second * 10

func (session session) onceOver(
	group string, pattern *regexp.Regexp, overSpec string,
) error {
	lines, err := session.client.Over(overSpec)
	logger.Printf("Response OVER: %#v", lines)
	if err != nil {
		return err
	}
	for i, line := range lines {
		parts := strings.Split(line, "\t")
		artN, subject := parts[0], parts[1]
		if !pattern.MatchString(subject) {
			if session.isRefless(group) {
				if err := session.addLast(group, artN); err != nil {
					return err
				}
				continue
			}
			indirect, err := session.seenAny(strings.Fields(parts[5])...)
			if err != nil {
				return err
			}
			if !indirect {
				if err := session.addLast(group, artN); err != nil {
					return err
				}
				continue
			}
		}
		logger.Printf("Match %s %s /%s/: %s", artN, group, pattern, subject)
		parts = parts[1:]
		if err = session.processArticle(artN, group, parts); err != nil {
			return err
		}
		if i != len(lines) - 1 {
			time.Sleep(floodStop)
		}
	}
	return nil
}

func (session session) processGroup(
	group string, pattern *regexp.Regexp,
) error {
	client := session.client

	// Select a group
	g, err := client.Group(group)
	if err != nil {
		return err
	}
	logger.Printf("Group: %#v", g)

	// Deal with deferred articles first
	if err := crud.IterDeferred(
		session.store, group, session.createDeferredHandler(group, pattern),
	); err != nil {
		return err
	}

	// Find last seen
	last, err := session.getLast(&g)
	if err != nil {
		return err
	}
	if last >= g.High {
		logger.Printf("Nothing to do for %s, already at %d: %#v", group, last, g)
		return nil
	}

	// Get summaries
	spec := fmt.Sprintf("%d-", last+1)
	if err = session.onceOver(group, pattern, spec); err != nil {
		return err
	}
	return nil
}

func (session session) summarizeDb() error {
	return crud.Summarize(session.store, logger)
}

func maybeGetFields(client *nntpclient.Client) ([]string, error) {
	hasOver, err := client.HasCapabilityArgument("LIST", "OVERVIEW.FMT")
	if err != nil {
		return nil, err
	}
	if hasOver {
		fields, err := client.ListOverviewFmt()
		if err != nil {
			return nil, err
		}
		return fields, nil
	} else if client.GetCapability("OVER") == "" {
		return nil, errors.New("Server support for OVER is required")
	}
	return nil, nil
}

func (session session) queryService() error {
	client, err := session.connect()
	if err != nil {
		return err
	}
	defer client.Close()
	session.client = client
	logger.Printf("Banner: %v", client.Banner)

	caps, err := client.Capabilities()
	if err != nil {
		return err
	}
	logger.Printf("Caps advertised: %v", caps)

	if !client.HasTLS() && client.GetCapability("STARTTLS") == "STARTTLS" {
		logger.Println("Upgrading to TLS")
		config, err := CreateSimpleTLSConfig(session.server)
		if err != nil {
			return err
		}
		if err := client.StartTLS(config); err != nil {
			return err
		}
	}

	if err = session.maybeAuth(); err != nil {
		return err
	}

	if fields, err := maybeGetFields(client); err != nil {
		return err
	} else {
		logger.Printf("Fields: %v", fields)
		session.fields = fields // may be nil
	}

	if _, _, err = client.Command("MODE READER", 2); err != nil {
		return err
	}

	// Request and handle new articles
	for group, pattern := range session.patterns {
		logger.Printf("Processing %s for /%s/", group, pattern)
		if err = session.processGroup(group, pattern); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	session, err := newSession()
	if err != nil {
		logger.Fatalf("Error loading db: %v", err)
	}
	defer session.store.Close()

	crud.SortDeferredFunction = crud.SortDeferredByKey

	for {
		// TODO get backoff mechanism working
		timeoutInterval := session.period
		if err = session.queryService(); err != nil {
			logger.Printf("Fatal error: %v", err)
			timeoutInterval *= 2
		}
		if timeoutInterval == 0 {
			break
		}
		logger.Printf("Going again in %s", timeoutInterval.String())
		time.Sleep(timeoutInterval)
	}
	if err != nil {
		os.Exit(1)
	}
	if err = session.summarizeDb(); err != nil {
		logger.Fatalf("Error summarizing db: %v", err)
	}
}
