package main

import (
	"os"
	"os/exec"
	"strconv"

	"gitlab.com/jpneverwas/nntp-crier/internal/crud"
)

type ecHandler = func(session, *exec.ExitError, string, string, string) error
var ecHandlers map[int]ecHandler

// Exit Code handlers decide what to do based on the external handler's output
//
// For more granular control have the handler emit messages on stderr.
//
// XXX making these session methods (a more natural fit) causes a segfault
// in the bunt transaction: DB.Update -> DB.managed -> DB.Begin -> Tx.lock

func ecOK(session session, _ *exec.ExitError, art, grp, id string) error {
	if err := session.maybeDeleteDeferred(grp, art); err != nil {
		return err
	}
	if err := session.rememberId(id, grp); err != nil {
		return err
	}
	if err := session.addLast(grp, art); err != nil {
		return err
	}
	return nil
}

func ecPunt(session session, err *exec.ExitError, art, grp, _ string) error {
	if err := session.addLast(grp, art); err != nil {
		return err
	}
	logger.Printf("Handler punted: err: %s, output: %s\n",
		err.Error(), string(err.Stderr))
	return nil
}

func ecDefer(session session, err *exec.ExitError, art, grp, _ string) error {
	// Add to watch list unless already thur
	if v, err := crud.GetDeferredItem(session.store, grp, art); err != nil {
		return err
	} else if v == nil {
		exp := getExpiration(session.deferPeriod)
		if err := crud.AddDeferredItem(session.store, grp, art, &exp);
		err != nil {
			return err
		}
	}
	if len(err.Stderr) > 0 {
		logger.Printf("Handler deferred: %s\n", string(err.Stderr));
		return nil
	}
	logger.Println("Handler deferred");
	return  nil
}

func (session session) setECHandler(
	name string, def int, f ecHandler,
) error {
	if code := os.Getenv(name); code != "" {
		if i, err := strconv.ParseInt(code, 10, 0); err != nil {
			return err
		} else {
			ecHandlers[int(i)] = f
		}
	} else {
		ecHandlers[def] = f
	}
	return nil
}

