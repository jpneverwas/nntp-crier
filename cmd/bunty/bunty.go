package main

import (
	"flag"
	"fmt"

	"gitlab.com/jpneverwas/nntp-crier/internal/crud"
)

var flagDb string
var flagGet string
var flagSet string
var flagDelete string
var flagList bool

func main() {
	flag.StringVar(&flagDb, "db", "", "Path to database")
	flag.BoolVar(&flagList, "list", false, "Print the database")
	flag.StringVar(&flagSet, "set", "", "Add or alter current entry")
	flag.StringVar(&flagGet, "get", "", "Retrieve current entry")
	flag.StringVar(&flagDelete, "delete", "", "Remove current entry")
	flag.Parse()
	if flagDb == "" {
		flag.Usage()
		return
	}
	db, err := crud.Open(flagDb)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	if flagList {
		crud.Summarize(db, nil)
		return
	}
	if flagDelete != "" {
		if val, err := crud.Delete(db, flagDelete); err != nil {
			panic(err)
		} else {
			fmt.Printf("Deleted %s: %s\n", flagDelete, val)
		}
		return
	}
	if flagSet != "" {
		val := flag.Arg(0)
		if prev, replaced, err := crud.Set(db, flagSet, val); err != nil {
			panic(err)
		} else {
			verb := "Set"
			if replaced {
				verb = "Replaced"
			}
			fmt.Printf("%s %s %#v -> %#v\n", verb, flagSet, prev, val)
		}
		return
	}
	if flagGet != "" {
		if found, err := crud.Get(db, flagGet); err != nil {
			panic(err)
		} else {
			fmt.Printf("key: %s, value: %s\n", flagGet, found)
		}
		return
	}
}
