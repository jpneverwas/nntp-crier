package transpo

import (
	"fmt"
	"net"
)

func FmtTCP(host string, port int) string {
	return fmt.Sprintf("%s:%d", host, port)
}

// CatOutput sends a message to an IRCcat-like TCP endpoint.
func CatOutput(target, address string, message []byte) error {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return err
	}
	msg := append([]byte(target), ' ')
	msg = append(msg, message...)
	msg = append(msg, '\n')
	_, err = conn.Write(msg)
	if err != nil {
		return err
	}
	return nil
}

