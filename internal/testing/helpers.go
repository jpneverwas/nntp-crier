package helpers

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"testing"
)

var HelperEnvvar = "GO_WANT_HELPER_PROCESS"
var MainTestName = "TestMain"

func Compare(expect, result []byte) error {
	rv := bytes.Compare(expect, result)
	var msg string
	if rv != 0 {
		for i := 0; ; i++ {
			if i == len(expect) {
				msg = "Result is longer than expected"
				break
			}
			if i == len(result) {
				msg = "Result is too short"
				break
			}
			if result[i] != expect[i] {
				msg = fmt.Sprintf(
					"@char # %d\nexpect < %02x:'%c'\nresult > %02x:'%c'\n",
					i, expect[i], expect[i], result[i], result[i],
				)
				break
			}
		}
		return fmt.Errorf(
			"%s\nmismatch %d:\n\n%s\n=====\n%s", msg, rv, expect, result,
		)
	}
	return nil
}


// RunAsChild runs the test in a subprocess.
func RunAsChild(
	t *testing.T, name, srcFile, expectFile string, exe func(),
) {
	if os.Getenv(HelperEnvvar) == name {
		os.Args = append(os.Args[:1], "-source", srcFile)
		exe()
		os.Exit(0)
	}
	var cmd *exec.Cmd
	cmd = exec.Command(os.Args[0], "-test.run=" + t.Name())
	cmd.Env = append(os.Environ(), fmt.Sprintf("%s=%s", HelperEnvvar, name))
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	outerr := stderr.Bytes()
	if err != nil {
		t.Fatalf("Failed to spawn child process: %v\n%s", err, string(outerr))
	}
	output := stdout.Bytes()
	expect, err := os.ReadFile(expectFile)
	if err != nil {
		t.Fatal(err)
	}
	if err := Compare(expect, output); err != nil {
		if len(outerr) != 0 {
			t.Logf("stderr:\n%s", string(outerr))
		}
		t.Fatal(err)
	}
}
