package crud

import (
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"time"

	"github.com/tidwall/buntdb"
)

// Set updates the database with a new value for the given key.
// It returns the previous value and confirmation of replacement.
func Set(db *buntdb.DB, key, val string) (string, bool, error) {
	var prev string
	var replaced bool
	err := db.Update(func(tx *buntdb.Tx) error {
		p, r, err := tx.Set(key, val, nil)
		prev, replaced = p, r
		return err
	})
	return prev, replaced, err
}

// SetOne writes a k/v pair to the database and returns an error.
func SetOne(db *buntdb.DB, name, num string) error {
	_, _, err := Set(db, name, num)
	return err
}

// Get returns an item's value or an error
func Get(db *buntdb.DB, key string) (string, error) {
	var found string
	err := db.View(func(tx *buntdb.Tx) error {
		value, err := tx.Get(key)
		if err != nil {
			return err
		}
		found = value
		return nil
	})
	if err != nil {
		return "", err
	}
	return found, nil
}

// Has returns true if item with key exists in database
func Has(db *buntdb.DB, key string) (bool, error) {
	var found string
	err := db.View(func(tx *buntdb.Tx) error {
		value, err := tx.Get(key)
		if err != nil && err.Error() != "not found" {
			return err
		}
		found = value
		return nil
	})
	if err != nil {
		return false, err
	}
	return found != "", nil
}

func HasAny(db *buntdb.DB, keys... string) (bool, error) {
	for _, key := range keys {
		found, err := Has(db, key)
		if err != nil {
			return false, err
		}
		if found {
			return found, nil
		}
	}
	return false, nil
}

type Printer interface {
	Printf(format string, v ...interface{})
}

func Summarize(db *buntdb.DB, printer Printer) error {
	return db.View(func(tx *buntdb.Tx) error {
		return tx.Ascend("", func(key, value string) bool {
			if printer != nil {
				printer.Printf("key: %s, value: %s\n", key, value)
			} else {
				fmt.Printf("key: %s, value: %s\n", key, value)
			}
			return true
		})
	})
}

var Open func (path string) (*buntdb.DB, error) = buntdb.Open

func Delete(db *buntdb.DB, key string) (string, error) {
	var val string
	err := db.Update(func(tx *buntdb.Tx) error {
		s, err := tx.Delete(key)
		val = s
		return err
	})
	if err != nil {
		return "", err
	}
	return val, nil
}

// Deferred maps a string (below called "id") to a time.Time object
type Deferred map[string]time.Time

var DeferredSuffix = "deferred"

type deferredEntry struct {
	key string
	value time.Time
}

func (d Deferred) asEntries() (td []deferredEntry) {
	td = []deferredEntry{}
	for k, v := range d {
		td = append(td, deferredEntry{k, v})
	}
	return
}

// Return new or existing id -> expiration Deferred map
func getOrCreateDeferred(db *buntdb.DB, key string) (Deferred, error) {
	dkey := key + ":" + DeferredSuffix
	existing, err := Get(db, dkey)
	if err != nil && err.Error() != "not found" {
		return nil, err
	}
	var deferred Deferred
	if existing != "" {
		if err = json.Unmarshal([]byte(existing), &deferred); err != nil {
			return nil, err
		}
	} else {
		deferred = make(Deferred)
	}
	return deferred, nil
}

// setDeferred overwrites Deferred map stored under key with new/updated map.
func setDeferred(db *buntdb.DB, key string, deferred Deferred) error {
	dkey := key + ":" + DeferredSuffix
	entry, err := json.Marshal(deferred)
	if err != nil {
		return err
	}
	err = SetOne(db, dkey, string(entry))
	if err != nil {
		return err
	}
	return nil
}

// GetDeferredItem retrieves an item "id" of Deferred map under "key". The
// "key" is and unsuffixed database key. The suffix DeferredSuffix is appended
// during lookup.
func GetDeferredItem(db *buntdb.DB, key, id string) (*time.Time, error) {
	d, err := getOrCreateDeferred(db, key)
	if err != nil {
		return nil, err
	}
	v, ok := d[id]
	if ok {
		return &v, nil
	}
	return nil, nil
}

func AddDeferredItem(db *buntdb.DB, key, id string, val *time.Time) error {
	d, err := getOrCreateDeferred(db, key)
	if err != nil {
		return err
	}
	_, ok := d[id]
	if ok {
		return errors.New("Item already exists")
	}
	d[id] = *val
	err = setDeferred(db, key, d)
	if err != nil {
		return err
	}
	return nil
}

func DeleteDeferredItem(db *buntdb.DB, key, id string) error {
	d, err := getOrCreateDeferred(db, key)
	if err != nil {
		return err
	}
	_, ok := d[id]
	if !ok {
		return errors.New("Item missing")
	}
	delete(d, id)
	err = setDeferred(db, key, d)
	if err != nil {
		return err
	}
	return nil
}

type ItemHandler func(Deferred, string, time.Time) error

func SortDeferredAsAdded(td []deferredEntry) {
	sort.Slice(td, func(i, j int) bool {
		return td[i].value.Before(td[j].value)
	})
}

func SortDeferredByKey(td []deferredEntry) {
	sort.Slice(td, func(i, j int) bool {
		return td[i].key < td[j].key
	})
}

var SortDeferredFunction = SortDeferredAsAdded

// IterDeferred calls a handler with every deferred key/value pair.
// It does visits them in the order determined by calling SortDeferredFunction.
func IterDeferred(db *buntdb.DB, key string, handleItem ItemHandler) error {
	d, err := getOrCreateDeferred(db, key)
	if err != nil {
		return err
	}
	wasEmpty := len(d) == 0
	td := d.asEntries()
	SortDeferredFunction(td)
	for _, thing := range td {
		if err := handleItem(d, thing.key, thing.value); err != nil {
			return err
		}
	}
	if wasEmpty && len(d) == 0 {
		return nil
	}
	if err := setDeferred(db, key, d); err != nil {
		return err
	}
	return nil
}
