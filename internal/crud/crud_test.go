package crud

import (
	"fmt"
	"testing"
	"time"

	"github.com/tidwall/buntdb"
)

func TestGet(t *testing.T) {
	db, err := buntdb.Open(":memory:")
	if err != nil {
		t.Errorf("%v", err)
	}
	err = SetOne(db, "foo", "bar")
	if err != nil {
		t.Errorf("%v", err)
	}
	defer db.Close()
	rv, err := Get(db, "foo")
	if err != nil || rv != "bar" {
		t.Errorf("%v", err)
	}
	rv, err = Get(db, "fake")
	if err == nil {
		t.Error("Did not raise error")
	}
}

func TestGetDeferredItem(t *testing.T) {
	db, err := buntdb.Open(":memory:")
	if err != nil {
		t.Errorf("%v", err)
	}
	now := time.Now()
	err = AddDeferredItem(db, "my.key", "<some.id>", &now)
	if err != nil {
		t.Errorf("%v", err)
	}
	val, err := GetDeferredItem(db, "my.key", "<some.id>")
	if err != nil {
		t.Errorf("%v", err)
	}
	if ! now.Equal(*val) {
		t.Errorf("expected %v, got %v", now, val)
	}
	err = DeleteDeferredItem(db, "my.key", "<some.id>")
	if err != nil {
		t.Errorf("%v", err)
	}
	val, err = GetDeferredItem(db, "my.key", "<some.id>")
	if err != nil {
		t.Errorf("%v", err)
	}
	if val != nil {
		t.Error("Failed to delete id")
	}
}

func runIterDeferred(t *testing.T) []string {
	db, err := buntdb.Open(":memory:")
	if err != nil {
		t.Errorf("%v", err)
	}
	now := time.Now()
	err = AddDeferredItem(db, "my.key", "<foo.id>", &now)
	if err != nil {
		t.Errorf("%v", err)
	}
	later := time.Now()
	err = AddDeferredItem(db, "my.key", "<bar.id>", &later)
	if err != nil {
		t.Errorf("%v", err)
	}
	laterStill := time.Now()
	err = AddDeferredItem(db, "my.key", "<baz.id>", &laterStill)
	if err != nil {
		t.Errorf("%v", err)
	}

	seen := []string{}

	if err := IterDeferred(
		db, "my.key", func (d Deferred, k string, exp time.Time) error {
			seen = append(seen, k)
			if exp.Before(time.Now()) {
				delete(d, k)
				return nil
			}
			return fmt.Errorf("Unexpected expiration for %s", k)
		},
	); err != nil {
		t.Error("IterDeferred failed")
	}

	val, err := GetDeferredItem(db, "my.key", "<foo.id>")
	if err != nil {
		t.Errorf("%v", err)
	}
	if val != nil {
		t.Error("Failed to delete id")
	}
	return seen
}

func TestIterDeferredByKey(t *testing.T) {
	seen := runIterDeferred(t)
	if seen[0] != "<foo.id>" || seen[1] != "<bar.id>" || seen[2] != "<baz.id>"{
		t.Error("Bad ordering")
	}
	SortDeferredFunction = SortDeferredByKey
	seen = runIterDeferred(t)
	if seen[0] != "<bar.id>" || seen[1] != "<baz.id>" || seen[2] != "<foo.id>"{
		t.Error("Bad ordering")
	}
}
