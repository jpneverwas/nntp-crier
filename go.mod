module gitlab.com/jpneverwas/nntp-crier

go 1.16

require (
	github.com/antchfx/htmlquery v1.2.3
	github.com/dustin/go-nntp v0.0.0-20210723005859-f00d51cf8cc1
	github.com/mmcdole/gofeed v1.1.3
	github.com/tidwall/buntdb v1.2.4
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e
	golang.org/x/tools v0.1.4
)
