# What
A service that queries NNTP servers for new articles and invokes an external
handler when subject lines match per-group patterns. **TODO**: allow matching
against *any* `OVER` fields (coming soon, along with file-based
configuration).

# Usage
Command line args of the form `<group>=<pattern>` will have their `<pattern>`s
tested against unseen `<group>` articles. `<pattern>` can be anything accepted
by Go's regexp module (taking care to escape backslashes in YAML, etc.). When
adding a new group, only the two most recent articles will be considered new.

## Environment variables
Any variables below not marked *optional* are required.

### `CRIER_HANDLER`
Path to executable match handler. When there's a match, the exe will be called
with args similar to:

```
1. misc.test
2. Subject: Re: Some example
3. From: John Doe <john@doe.com>
4. Date: Wed, 23 Jun 2021 09:02:48 +0200
5. Message-Id: <42b8aae2d.pnzry@gmail.com>
6. References: <348383991.ok@example.com> <993851923.foo@example.org>
7. :bytes 930
8. :lines 42
9. X-Foo: hello
10. X-Bar: world
```

Handlers are called synchronously. The full article, including headers, is
piped to standard in. Nonzero exit codes unclaimed by a `CRIER_EC_*` env var
(see below) will cause the program to abort the current cycle; and processing
will begin anew from the first group after the designated timeout.

### `CRIER_DATABASE`
Path to some location on the file system. This file won't grow very large and
can probably be left unattended forever. Use one per server.

### `CRIER_SERVER`
Server address in TCP format. For example: `news.gmane.io:119`. If the port
is 563, TLS is turned on. Otherwise, `STARTTLS` is used when available.

### `CRIER_USER/CRIER_PASS`
*Optional*. Two separate vars for authenticating to the news server.

### `CRIER_PERIOD`
*Optional*. Polling interval indicating how long to sleep between updates. If
not provided, exit after querying once. For now, this program lacks a smart
backoff mechanism to try again after a failure; it just waits twice as long
before continuing. Acceptable units are "s", "m", "h".

### `CRIER_REFLESS`
*Optional* A comma-separated list of groups whose references should be ignored
when a subject doesn't match.

### `SOCKS_PROXY`
*Optional*. Something like `socks5h://localhost:9050`. Note that this
is nonstandard. The variable used in Go programs tends to be `ALL_PROXY`.

### `CRIER_DEFER_PERIOD`
*Optional*. Grace period during which to retry processing deferred articles
for a given group, if any. When period elapses, articles are forgotten.
Defaults to 6 h. Acceptable units are "s", "m", "h".

### `CRIER_EC_PUNT`
*Optional*. Handler error code indicating that the program should effectively
ignore the article. It does this by advancing the last-seen article number for
the group but not marking the message ID as significant. The default is 31.

### `CRIER_EC_DEFER`
*Optional*. Handler error code indicating that the program should behave much
like it does when "punting" except that the selected article is placed in a
retry bucket and processed again periodically until the deferment period has
ended. Defaults to 42.


# FAQ/Misc

## I want built-in network RPC instead of an external handler
Less "external" solutions include bind-mounting a self-contained binary
handler in the provided container or committing a new image with all the
dependencies needed. This is much simpler than supporting 31 flavors of RPC.

## What about when subject lines change mid-thread?
The IDs of all accepted messages are recorded permanently. References to those
IDs are considered automatic matches.


# RFCs
1. [Original](https://datatracker.ietf.org/doc/html/rfc977)
2. [Revised](https://datatracker.ietf.org/doc/html/rfc3977)
3. [XOVER](https://datatracker.ietf.org/doc/html/rfc2980)
