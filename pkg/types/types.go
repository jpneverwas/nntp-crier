package types

import (
	"bytes"
	"io"
	"net/mail"
)

type Header interface{
	Get(string) string
	AddressList(string) ([]*mail.Address, error)
}

// MediaHandler provides the tools to process a message to completion
//
// When called with existing output, this method may choose to overwrite some
// portion or just append, if room allows.
type MediaHandler interface{
	Handle() error
	GetHeader() Header
}

type MediaFilter func(MediaHandler, []byte)[]byte

type MediaHandlerFactory func(
	Header, io.Reader, *bytes.Buffer, ...MediaFilter,
) (MediaHandler, error)

type MediaHandlerStorage interface{
	MediaHandler
	GetStorage() map[string]interface{}
}

type MediaHandlerStated interface{
	MediaHandler
	GetAddressees() []*mail.Address
	GetBuffer() *bytes.Buffer
	GetLineNumber() int
	GetWrites() []int
}

