package filter

import (
	"net/mail"
	"net/textproto"
	"regexp"
	"strings"

	"gitlab.com/jpneverwas/nntp-crier/pkg/types"
)

var GmanePat = regexp.MustCompile("__\\d+[.]\\d+[$]\\d{10}[$]gmane[$]org")

var viaPat = regexp.MustCompile("^(.+) via .+$")

func checkHeader(h types.Header) (string, bool) {
	from, err := h.AddressList("from")
	if err != nil {
		panic(err)
	}
	addr := from[0]
	if subs := viaPat.FindStringSubmatch(addr.Name); len(subs) != 0 {
		return subs[1] + " <" + addr.Address + ">", true
	}
	return "", false
}

// FilterVia is a no-op for now.
// Actually, it may stash a short version of the "from" name for later.
func FilterVia(mh types.MediaHandlerStorage, line []byte) []byte {
	storage := mh.GetStorage()
	if _, ok := storage["via"]; ok {
		return line
	}
	storage["via"] = ""
	maybeNew, ok := checkHeader(mh.GetHeader())
	if ok {
		storage["via"] =  maybeNew
	}
	return line
}

func MessageFilterVia(header mail.Header) {
	maybeNew, ok := checkHeader(mail.Header(header))
	if !ok {
		return
	}
	textproto.MIMEHeader(header).Set("from", maybeNew)
	return
}

func hasAttribution(addrs []*mail.Address, s string) bool {
	for _, addr := range addrs {
		if strings.Contains(s, addr.Name) ||
			strings.Contains(s, addr.Address) {
			return true
		}
	}
	return false
}

func FilterWrites(mh types.MediaHandlerStated, line []byte) []byte {
	replacement := []byte{}
	s := string(line)
	if !strings.HasSuffix(s, "writes:") &&
		!strings.HasSuffix(s, "wrote:") &&
		!strings.HasSuffix(s, "said:") {
		return line
	}
	writes := mh.GetWrites()
	// Maybe erase last line
	if mh.GetLineNumber() < 3 && len(writes) != 0 {
		// Hm, if we only expect to encounter split John Doe\nwrites: at the
		// start of a message body, then lastWrite == mh.GetBuffer().Len() is
		// an invariant, and we can forgo tracking writes.
		lastWrite := writes[len(writes) - 1]
		lastSlice := mh.GetBuffer().Len() - lastWrite
		lastLine := mh.GetBuffer().Bytes()[lastSlice:]
		if hasAttribution(mh.GetAddressees(), string(lastLine)) {
			mh.GetBuffer().Truncate(lastSlice)
		}
	}
	if mh.GetLineNumber() < 6 {
		return replacement
	}
	// Apply some heuristics, but only honor same line
	replacement = []byte("…")
	if strings.HasPrefix(s, "On ") {
		return replacement
	}
	if strings.HasPrefix(s, ">>") {
		return replacement
	}
	if hasAttribution(mh.GetAddressees(), s) {
		return replacement
	}
	return line
}
