package feed

// Note: at the moment, this only supports cgit/atom feeds due to some
// nonstandard behavior in cgit with regard to mapping commit/author dates to
// XML fields.

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/mmcdole/gofeed"
	"gitlab.com/jpneverwas/nntp-crier/pkg/compose"
	"golang.org/x/net/html"
)

type RenderInfo struct {
	Feed *gofeed.Feed
	Item *gofeed.Item
}

// DefaultTemplate is the default template used to render a message.
//
// This shows the author date instead of the commit date by default, even
// though the latter is used throughout for ordering and inclusion. When
// printed as a list, items may appear out of order, much like they do with
// git-log(1).
const DefaultTemplate = "[{{ ref .Feed.Title }}] " +
	"\x02{{ .Item.Author.Name }}\x02 " +
	"{{ date .Item.PublishedParsed }} " +
	"{{ link .Item.Link }} {{ .Item.Title }}{{ msg .Item.Content }}"

func dated(t time.Time) string {
	// TODO maybe drop +0000 because published time is UTC, but commit time's
	// tz is lost
	return t.Format(compose.DefaultDateFormatString)
}

// unPre looks for <pre> tags and extracts content if present.
func unPre(s string) string {
	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Println("Problem parsing XML content", err)
		return s
	}
	// From x/net/html example_test.go. This wraps pre stub in full HTML tree
	// before commencing.
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "pre" {
			s = n.FirstChild.Data
			return
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return s
}

func scrapeContent(s string) string {
	unpreed := unPre(s)
	words := strings.Fields(unpreed)
	joined := strings.Join(words, " ")
	if joined == "" {
		return ""
	}
	return " | " + joined
}

// scrapePathAndBranch extracts a path and branch from the <title>.
//
// It returns a tuple of path, revision (branch). Path may be empty. It's not
// obvious from the doc string in cgit_parse_url() in parsing.c, but "atom" is
// a cmd (see https://git.zx2c4.com/cgit/tree/cmd.c?id=5258c297b#n174), so you
// can do like https://git.zx2c4.com/cgit/atom/filters to get the feed from a
// subtree of some project. This returns something like ("", "master") or
// ("filters", "patch/foo").
func scrapePathAndBranch(s string) (path string, branch string) {
	parts := strings.SplitN(s, ", ", 2)
	fullPath, fullRev := parts[0], parts[1]
	// Ditch leading project root
	parts = strings.SplitN(fullPath, "/", 2)
	if len(parts) == 2 {
		path = parts[1]
	}
	parts = strings.SplitN(fullRev, " ", 2)
	if len(parts) == 2 {
		branch = parts[1]
	}
	return path, branch
}

// formRef returns [doc/html master], [html/master], or just [master].
// A ".git' suffix in the path portion is dropped.
func formRef(s string) string {
	path, branch := scrapePathAndBranch(s)
	if path != "" {
		if strings.HasSuffix(path, ".git") {
			path = path[:len(path) - 4]
		}
		if strings.Index(path, "/") == -1 {
			path = fmt.Sprintf("%s/", path)
		} else {
			path = fmt.Sprintf("%s ", path)
		}
	}
	return fmt.Sprintf("%s%s", path, branch)
}

// formRef returns [master·doc/html.git] or just [master].
// A ".git" suffix in the path portion is retained.
func formRefReversed(s string) string {
	path, branch := scrapePathAndBranch(s)
	if path != "" {
		path = fmt.Sprintf("·%s", path)
	}
	return fmt.Sprintf("%s%s", branch, path)
}

// Funcs are go-template helper functions for atom feeds.
var Funcs = template.FuncMap{
	"ref":  formRef,
	"refRev":  formRefReversed,
	"msg":  scrapeContent,
	"link": compose.ShortenRevision,
	"date": dated,
}

func createTmpl(
	content string, item *gofeed.Item, feed *gofeed.Feed,
) ([]byte, error) {
	tmpl, err := template.New("message").Funcs(Funcs).Parse(content)
	if err != nil {
		return []byte{}, err
	}
	out := bytes.Buffer{}
	err = tmpl.Execute(&out, RenderInfo{feed, item})
	return out.Bytes(), nil
}

type HTTPError struct {
	statusCode int
	status     string
}

func (err HTTPError) Error() string {
	return fmt.Sprintf("http error: %s", err.status)
}

func (err HTTPError) StatusCode() int {
	return err.statusCode
}

// FetchURL is gofeed/parser.go ParseURLWithContext
func FetchURL(feedURL string) (out []byte, err error) {
	resp, err := http.Get(feedURL)
	if err != nil {
		return out, err
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return out, HTTPError{
			statusCode: resp.StatusCode,
			status:     resp.Status,
		}
	}
	out, err = io.ReadAll(resp.Body)
	if err != nil {
		return out, err
	}
	return out, nil
}

func LoadLatest(saved string) (*gofeed.Feed, error) {
	f, err := os.Open(saved)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	fp := gofeed.NewParser()
	feed, err := fp.Parse(f)
	if err != nil {
		return nil, err
	}
	return feed, nil
}

func LoadFetched(fetched []byte) (*gofeed.Feed, error) {
	return gofeed.NewParser().ParseString(string(fetched))
}

// gofeedItems is a kludge to sort items.
//
// This gofeed library and the cgit app interpret <published> and <updated>
// fields differently. cgit sets <updated> as Author-Date and <published> as
// Commit-Date, but it should probably be the other way around. gofeed relies
// on published being the final word, but that's incompatible with the order
// in which revisions are applied.
//
// See also: https://git.zx2c4.com/cgit/commit/?h=ch/atom

type gofeedItems []*gofeed.Item

func (items gofeedItems) Len() int {
	return len(items)
}

// Note that Swap is conspicuously absent. We can't support the Sort interface
// because it's in-place (destructive), and the equals case requires looking
// up the original ordering to know which commit came first.
func (items gofeedItems) Less(i, k int) bool {
	return items[i].UpdatedParsed.Before(*items[k].UpdatedParsed)
}

func (items gofeedItems) reverse() {
	for i, j := 0, items.Len()-1; i < j; i, j = i+1, j-1 {
		items[i], items[j] = items[j], items[i]
	}
}

// Validate ensures the new feed is really newer.
func Validate(existing, current *gofeed.Feed) error {
	if existing.Link != current.Link {
		return fmt.Errorf(
			"Endpoints do not match: want: %s got: %s",
			existing.Link, current.Link,
		)
	}
	n := existing.Len()
	if n == 0 {
		return fmt.Errorf("Existing is empty: %d", n)
	}
	if n > current.Len() {
		return fmt.Errorf(
			"Entry count shrank: want: %d, got: %d", n, current.Len(),
		)
	}
	// Ensure items appear in decreasing order according to commit time
	//
	// FIXME this is unnecessary because the upstream code guarantees it
	items := gofeedItems(current.Items)
	for i, o := range items {
		if i == 0 {
			continue
		}
		if items.Less(i-1, i) {
			return fmt.Errorf(
				"Unexpected ordering: [%d]%s != [%d]%s",
				i-1, current.Items[i-1].GUID, i, o.GUID,
			)
		}
	}
	return nil
}

// GetNew returns new entries after the one last announced.
// They are sorted from oldest to most recent.
func GetNew(existing, current *gofeed.Feed) (new gofeedItems) {
	last := existing.Items[0].UpdatedParsed
	for _, o := range current.Items {
		if !last.Before(*o.UpdatedParsed) {
			break
		}
		new = append(new, o)
	}
	new.reverse()
	return new
}

// RenderMessage executes a given template and trims it.
func RenderMessage(
	tmplStr string, max int, item *gofeed.Item, feed *gofeed.Feed,
) (out []byte, err error) {
	msg, err := createTmpl(tmplStr, item, feed)
	if err != nil {
		return out, err
	}
	return compose.Trim(msg, max), nil
}

func SaveNewLatest(savePath string, body []byte) error {
	existing, err := os.ReadFile(savePath)
	if err != nil {
		return err
	}
	err = os.WriteFile(fmt.Sprintf("%s.bak", savePath), existing, 0600)
	if err != nil {
		return err
	}
	return os.WriteFile(savePath, body, 0600)
}
