package feed

import (
	"os"
	"sort"
	"testing"

	helpers "gitlab.com/jpneverwas/nntp-crier/internal/testing"
)


func TestValidate(t *testing.T) {
	existing, err := LoadLatest("testdata/01.atom.xml")
	if err != nil {
		t.Fatal(err)
	}
	current, err := LoadLatest("testdata/02.atom.xml")
	if err != nil {
		t.Fatal(err)
	}
	if err = Validate(existing, current); err != nil {
		t.Fatal(err)
	}
}

// This is due to a regression
func TestValidateOrdering(t *testing.T) {
	existing, err := LoadLatest("testdata/emacs-01.atom.xml")
	if err != nil {
		t.Fatal(err)
	}
	current, err := LoadLatest("testdata/emacs-01.atom.xml")
	if err != nil {
		t.Fatal(err)
	}
	if err = Validate(existing, current); err != nil {
		t.Fatal(err)
	}
	sort.Sort(current)
	if err = Validate(existing, current); err == nil {
		t.Fatalf("Expected error")
	}
	existing, err = LoadLatest("testdata/poke-maint-2-01.xml")
	if err != nil {
		t.Fatal(err)
	}
	current, err = LoadLatest("testdata/poke-maint-2-02.xml")
	if err != nil {
		t.Fatal(err)
	}
	if err = Validate(existing, current); err != nil {
		t.Fatal(err)
	}
	sort.Sort(current)
	if err = Validate(existing, current); err == nil {
		t.Fatalf("Expected error")
	}
}

func TestGetNew(t *testing.T) {
	existing, err := LoadLatest("testdata/01.atom.xml")
	if err != nil {
		t.Fatal(err)
	}
	c2, err := LoadLatest("testdata/02.atom.xml")
	if err != nil {
		t.Fatal(err)
	}
	if got := GetNew(existing, c2); len(got) != 1 {
		t.Fatalf("Wrong number of new articles: %d", len(got))
	}
	c3, err := LoadLatest("testdata/03.atom.xml")
	if err != nil {
		t.Fatal(err)
	}
	got := GetNew(existing, c3)
	if len(got) != 3 {
		t.Fatalf("Wrong number of new articles: %d", len(got))
	}
	if got[0].Title != "One more time for the record" {
		t.Fatalf("Wrong order: %s", got[0].Title)
	}
	if got[1].Title != "pyrophyllite" {
		t.Fatalf("Wrong order: %s", got[1].Title)
	}
	if got[2].Title != "paragglutination" {
		t.Fatalf("Wrong order: %s", got[2].Title)
	}
	if got := GetNew(c2, c3); len(got) != 2 {
		t.Fatalf("Wrong number of new articles: %d", len(got))
	}
	if got := GetNew(c3, c2); len(got) != 0 {
		t.Fatalf("Wrong number of new articles: %d", len(got))
	}
}

func TestScrapePathAndBranch(t *testing.T) {
	p, b := scrapePathAndBranch("poke, branch master")
	if p != "" || b != "master" {
		t.Fatalf("Unexpected result. Got p:%q, b:%q", p, b)
	}
	p, b = scrapePathAndBranch("cgit/filters, branch master")
	if p != "filters" || b != "master" {
		t.Fatalf("Unexpected result. Got p:%q, b:%q", p, b)
	}
}


func TestRenderMessage(t *testing.T) {
	feed, err := LoadLatest("testdata/04.atom.xml")
	if err != nil {
		t.Fatal(err)
	}
	got, err := RenderMessage(DefaultTemplate, 369, feed.Items[0], feed)
	if err != nil {
		t.Fatal(err)
	}
	expect, err := os.ReadFile("testdata/04.atom.expect")
	if err != nil {
		t.Fatal(err)
	}
	if err := helpers.Compare(expect, got); err != nil {
		t.Fatal(err)
	}
}

func TestRenderMessageRepo(t *testing.T) {
	feed, err := LoadLatest("testdata/poke-elf-master.xml")
	if err != nil {
		t.Fatal(err)
	}
	got, err := RenderMessage(DefaultTemplate, 369, feed.Items[0], feed)
	if err != nil {
		t.Fatal(err)
	}
	expect, err := os.ReadFile("testdata/poke-elf-master.expect")
	if err != nil {
		t.Fatal(err)
	}
	if err := helpers.Compare(expect, got); err != nil {
		t.Fatal(err)
	}
}
