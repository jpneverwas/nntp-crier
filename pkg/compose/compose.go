package compose

import (
	"bytes"
	"io"
	"log"
	"net/mail"
	"net/textproto"
	"net/url"
	"text/template"
	"mime"

	"gitlab.com/jpneverwas/nntp-crier/pkg/briefen"
)

var logger = log.Default()

const DefaultDateFormatString = "Mon, Jan 2 2006 15:04:05 -0700"

func renderDate(msg *mail.Message) string {
	date, err := mail.ParseDate(msg.Header.Get("date"))
	if err != nil {
		logger.Fatalf("%v", err)
	}

	return date.Format(DefaultDateFormatString)
}

func renderBody(msg *mail.Message) string {
	buf := &bytes.Buffer{}
	if err := briefen.Process(buf, msg); err != nil {
		logger.Fatalf("%v", err)
	}
	return string(buf.Bytes())
}

func renderFrom(msg *mail.Message) string {
	addr, err := mail.ParseAddress(msg.Header.Get("from"))
	if err != nil {
		logger.Fatal(err)
	}
	return addr.Name
}

func renderSubject(msg *mail.Message) string {
	dec := new(mime.WordDecoder)
	res, err := dec.DecodeHeader(msg.Header.Get("subject"))
	if err != nil {
		panic(err)
	}
	return res
}

var TmplFuncs template.FuncMap

func init() {
	TmplFuncs = template.FuncMap{}
	TmplFuncs["date"] = renderDate
	TmplFuncs["body"] = renderBody
	TmplFuncs["from"] = renderFrom
	TmplFuncs["subject"] = renderSubject
}

const BasicTemplate = `~ %BOLD{{from .Msg}}%NORMAL ~ {{date .Msg}} ` +
	`~ {{.Url}} ● {{subject .Msg}} | {{body .Msg}}`

func createTmpl(tmpl string) (*template.Template, error) {
	return template.New("ircmessage").Funcs(TmplFuncs).Parse(tmpl)
}

type Data struct {
	Msg *mail.Message
	Url string
}

func Trim(b []byte, max int) (out []byte) {
	if len(b) < max {
		return b
	}
	for _, r := range string(b) {
		if len(out) >= (max - 3) {
			break
		}
		out = append(out, []byte(string(r))...)
	}
	out = append(out, '.', '.', '.')
	return out
}

func trim(b *bytes.Buffer, max int) []byte {
	out := b.Bytes()
	if max != -1 && b.Len() > max {
		out = Trim(out, max)
	}
	return out
}

func SetHeader(header mail.Header, key, value string) {
	textproto.MIMEHeader(header).Set(key, value)
}

// Render renders.
// Param max means maximum number of bytes to truncate message to.
func Render(out io.Writer, msg *mail.Message, tmplRaw, url string, max int) error {
	tmpl := template.Must(createTmpl(tmplRaw))
	data := Data{Msg: msg, Url: url}
	buf := &bytes.Buffer{}
	if err := tmpl.Execute(buf, data); err != nil {
		return err
	}
	_, err := out.Write(trim(buf, max))
	if err != nil {
		return err
	}
	return nil
}

// ShortenRevision takes a URL with a SHA1 query param of "id" and truncates it.
func ShortenRevision(s string) string {
	parsed, err := url.Parse(s)
	if err != nil {
		return s
	}
	q := parsed.Query()
	id, ok := q["id"]
	if ok {
		parsed.RawQuery = "id=" + id[0][:8]
	}
	return parsed.String()
}
