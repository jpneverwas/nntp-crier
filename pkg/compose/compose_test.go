package compose

import (
	"testing"
	"net/mail"
)

func TestSetHeaders(t *testing.T) {
	h := mail.Header{}
	SetHeader(h, "key", "value")
	got := h.Get("key")
	if got != "value" {
		t.Fatal("failed")
	}
}
