package briefen

import (
	"bytes"
	"io"
	"log"
	"mime"

	. "gitlab.com/jpneverwas/nntp-crier/pkg/types"
)

var ContentHandlers map[string]MediaHandlerFactory
var ContentFilters map[string][]MediaFilter

// MHOther provides a fallback for unknown media types
type MHOther struct{
	h Header
	f io.Reader
}

func (mh *MHOther) GetHeader() Header {
	return mh.h
}

func (mh *MHOther) Handle() error {
	// _ is "params," which contains k/v pairs, like charset, etc.
	mtype, _, err := mime.ParseMediaType(mh.h.Get("content-type"))
	if err != nil {
		return err
	}
	// Throw away body robustly.
	// From https://datatracker.ietf.org/doc/html/rfc2045#section-6.7:
	//
	// (4)   Control characters other than TAB, or CR and LF as
	//       parts of CRLF pairs, must not appear. The same is true
	//       for octets with decimal values greater than 126.  If
	//       found in incoming quoted-printable data by a decoder, a
	//       robust implementation might exclude them from the
	//       decoded data and warn the user that illegal characters
	//       were discovered.
	//
	if _, err = io.ReadAll(mh.f); err != nil {
		return err
	}
	log.Default().Println("Unhandled Content-Type:", mtype)
	return nil
}

func CreateOther(
	h Header, f io.Reader, buf *bytes.Buffer, _filters... MediaFilter,
) (MediaHandler, error) {
	return &MHOther{h, f}, nil
}
