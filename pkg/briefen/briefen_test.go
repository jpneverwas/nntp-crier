package briefen

import (
	"bytes"
	"net/mail"
	"os"
	"testing"
	. "gitlab.com/jpneverwas/nntp-crier/pkg/types"

	helpers "gitlab.com/jpneverwas/nntp-crier/internal/testing"
)

// Regression
func TestGetBoundary(t *testing.T) {
	h := mail.Header{
		"To": []string{"Someone Cool <someone@example.cool>"},
	}
	s, err := getBoundary(h)
	if err != nil {
		t.Fatal(err)
	}
	if s != "" {
		t.Fatalf("Expected empty string, not %s", s)
	}
	// Only interested in mixed
	h["Content-Type"] = []string{"text/plain"}
	s, err = getBoundary(h)
	if err != nil {
		t.Fatal(err)
	}
	if s != "" {
		t.Fatalf("Expected empty string, not %s", s)
	}
	h["Content-Type"] = []string{"multipart/mixed; boundary=\"foo\""}
	s, err = getBoundary(h)
	if err != nil {
		t.Fatal(err)
	}
	if s != "foo" {
		t.Fatalf("Unexpected value: %s", s)
	}
}

func TestProcessAlt(t *testing.T) {
	ContentHandlers = make(map[string]MediaHandlerFactory)
	ContentHandlers["text/plain"] = CreateTextPlain
	ContentHandlers["multipart/alternative"] = CreateMultiAlt

	file, err := os.Open("testdata/multalt.email")
	if err != nil {
		t.Fatalf("%v", err)
	}
	msg, err := mail.ReadMessage(file)
	if err != nil {
		t.Fatalf("%v", err)
	}
	bound, err := getBoundary(msg.Header)
	if err != nil {
		t.Fatalf("%v", err)
	}
	buf := &bytes.Buffer{}
	if err = processParts(msg, bound, buf); err != nil {
		t.Fatalf("%v", err)
	}
	expect, err := os.ReadFile("testdata/multalt.expect")
	if err != nil {
		t.Fatal(err)
	}
	if err := helpers.Compare(expect, buf.Bytes()); err != nil {
		t.Fatal(err)
	}
	ContentHandlers = nil
}

func TestFFSReader(t *testing.T) {
	b := []byte("ab\fcdefg")
	r := &FFSReader{br: bytes.NewReader(b), rep: []byte{' '}}
	buf := []byte("1234")
	n, err := r.Read(buf)
	if err != nil {
		t.Fatal(err)
	}
	if n != 4 || string(buf) != "ab c" {
		t.Errorf("Expected 3, \"ab c\"; got %d, %q", n, buf)
	}
}
