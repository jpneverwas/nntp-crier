package briefen

import (
	"net/mail"
	"testing"
)

func TestSetHeader(t *testing.T) {
	mh := &MHTextPlain{
		h: mail.Header{},
	}
	mh.SetHeaderItem("foo", "bar")
	if mh.h.Get("foo") != "bar" {
		t.Fatal("unexpected result")
	}
}
