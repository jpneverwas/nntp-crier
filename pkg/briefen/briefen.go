// briefen reads an email message (including headers) and prints a portion of
// the body after applying some filters.
package briefen

import (
	"bytes"
	"io"
	"log"
	"mime"
	"mime/multipart"
	"net/mail"
	"strings"

	. "gitlab.com/jpneverwas/nntp-crier/pkg/types"
)

// getBoundary returns a multipart boundary when msg is multipart
func getBoundary(header mail.Header) (string, error) {
	mediaType, params, err := mime.ParseMediaType(header.Get("content-type"))
	if err != nil && !strings.Contains(err.Error(), "no media type"){
		return "", err
	}
	if ! strings.HasPrefix(mediaType, "multipart/") {
		return "", nil
	}
	return params["boundary"], nil
}

// cloneMap copies headers of parts over a copy of main headers
func cloneMap(headers ...map[string][]string) (mail.Header, error) {
	mergedHeaders := mail.Header{}
	for _, header := range headers {
		for k, v := range header {
			parts := make([]string, len(v))
			for i, part := range v {
				parts[i] = part
			}
			mergedHeaders[k] = parts
		}
	}
	return mergedHeaders, nil
}


// FFSReader is a reader that replaces form feeds with something else.
//
// Can't replace with "=0C" without altering the length.
type FFSReader struct {
	br io.Reader
	rep []byte
}

func (pf *FFSReader) Read(p []byte) (n int, err error) {
	n, err = pf.br.Read(p)
	for i, b := range bytes.ReplaceAll(p, []byte{0xC}, pf.rep) {
		p[i] = b
	}
	return n, err
}

// processParts iters over a multipart messasge, processing parts in turn
func processParts(msg *mail.Message, bound string, buf *bytes.Buffer) error {
	mr := multipart.NewReader(msg.Body, bound)
	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		contentType := p.Header.Get("content-type")
		if contentType == "" {
			continue
		}
		mediaType, _, err := mime.ParseMediaType(contentType)
		if err != nil {
			return err
		}
		maker, ok := ContentHandlers[mediaType]
		if ! ok {
			maker = CreateOther
		}
		filters, ok := ContentFilters[mediaType]
		if ! ok {
			filters = make([]MediaFilter, 0)
		}
		// multipart.nextPart deletes content-transfer-encoding
		merged, err := cloneMap(msg.Header, p.Header)
		if err != nil {
			return err
		}
		handler, err := maker(merged, p, buf, filters...)
		if err != nil {
			return err
		}
		if err = handler.Handle(); err != nil {
			return err
		}
	}
}

func Process(output io.Writer, msg *mail.Message) error {
	maxQPRetries := 2
	bodybuf := &bytes.Buffer{}
	_, err := io.Copy(bodybuf, msg.Body)
	bodybak := bodybuf.Bytes()
	msg.Body = bodybuf
	boundary, err := getBoundary(msg.Header)
	if err != nil {
		return err
	}
	buf := &bytes.Buffer{}
	if boundary == "" {
		filters, ok := ContentFilters["text/plain"]
		if ! ok {
			filters = make([]MediaFilter, 0)
		}
		handler, err := CreateTextPlain(
			msg.Header, msg.Body, buf, filters...,
		)
		if err != nil {
			return err
		}
		if err = handler.Handle(); err != nil {
			return err
		}
	} else {
		err = processParts(msg, boundary, buf)
	}
	if err != nil {
		if strings.HasPrefix(err.Error(), "quotedprintable: invalid") {
			if maxQPRetries != 0 && bytes.Contains(bodybak, []byte{0xc}) {
				maxQPRetries -= 1
				log.Default().Printf("%s\nTrying again...\n", err)
				b := make([]byte, len(bodybak))
				copy(b, bodybak)
				msg.Body = &FFSReader{br: bytes.NewReader(b), rep: []byte{' '}}
				buf.Reset()
				err = processParts(msg, boundary, buf)
			}
		}
		if err != nil {
			return err
		}
	}
	_, err = io.Copy(output, buf)
	if err != nil {
		return err
	}
	return nil
}

