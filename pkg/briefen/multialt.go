package briefen

import (
	"bytes"
	"errors"
	"io"
	"mime"
	"mime/multipart"
	. "gitlab.com/jpneverwas/nntp-crier/pkg/types"
)

// MHAlt provides handling for the multipart alternative content type
//
// See 7.2.3 The Multipart/alternative subtype
// https://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
//
type MHAlt struct{
	h Header
	f io.Reader
}

func (mh *MHAlt) GetHeader() Header {
	return mh.h
}

// This MH implementation wraps the most prefered subtype, which may just be
// the default no-op.
func (mh *MHAlt) Handle() error {
	return errors.New("Bad handle")
}

type altSection struct {
	mt string
	maker MediaHandlerFactory
	filters []MediaFilter
	r io.Reader
}
// AlternativeMediaTypes is a prioritized list of media types to render. For
// example, add "text/html" before or after "text/plain" if a corresponding
// handler exists in ContentHandlers, depending on which should be preferred.
var AlternativeMediaTypes = []string{"text/plain"}

// findPreferredAltSection returns the most preferred handler if any.
func findPreferredAltSection(
	sections []altSection, merged Header, buf *bytes.Buffer,
) (MediaHandler, error) {
	for _, mt := range AlternativeMediaTypes {
		for _, sec := range sections {
			if sec.mt == mt {
				handler, err := sec.maker(merged, sec.r, buf, sec.filters...)
				if err != nil {
					return nil, err
				}
				if err = handler.Handle(); err != nil {
					return nil, err
				}
				return handler, nil
			}
		}
	}
	return nil, nil
}

// getAltMaker gathers alternative sections for which we have a handler.
func getAltMaker(
	mr *multipart.Reader, merged Header, buf *bytes.Buffer,
) (MediaHandler, error) {
	sections := []altSection{}
	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		contentType := p.Header.Get("content-type")
		if contentType == "" {
			continue
		}
		mediaType, _, err := mime.ParseMediaType(contentType)
		if err != nil {
			return nil, err
		}
		maker, ok := ContentHandlers[mediaType]
		if !ok {
			continue
		}
		filters, ok := ContentFilters[mediaType]
		if ! ok {
			filters = make([]MediaFilter, 0)
		}
		r := &bytes.Buffer{}
		if _, err = io.Copy(r, p); err != nil {
			return nil, err
		}
		// Maybe merge filters?
		sections = append(sections, altSection{mediaType, maker, filters, r})
	}
	return findPreferredAltSection(sections, merged, buf)
}

func CreateMultiAlt(
	h Header, f io.Reader, buf *bytes.Buffer, filters... MediaFilter,
) (MediaHandler, error) {
	p, ok := f.(*multipart.Part)
	if !ok {
		return nil, errors.New("Non-mulitpart reader")
	}
	contentType := p.Header.Get("content-type")
	_, params, err := mime.ParseMediaType(contentType)
	if err != nil {
		return nil, err
	}
	boundary, ok := params["boundary"]
	if !ok { // impossible?
		return nil, errors.New("Alternative boundary not found")
	}
	mr := multipart.NewReader(p, boundary)
	handler, err := getAltMaker(mr, h, buf)
	if err != nil {
		return nil, err
	}
	if handler == nil {
		return CreateOther(h, f, buf, filters...)
	}
	return handler, nil
}
