package briefen

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"io"
	"mime/quotedprintable"
	"net/mail"
	"net/textproto"
	"strings"

	"gitlab.com/jpneverwas/nntp-crier/pkg/filter"
	. "gitlab.com/jpneverwas/nntp-crier/pkg/types"
)

type MHTextPlain struct{
	h Header
	f io.Reader
	b *bytes.Buffer
	ln int
	addrs []*mail.Address
	filters []func([]byte)[]byte
	extraFilters []MediaFilter
	extraStorage map[string]interface{}
	writes []int
}

func (mh *MHTextPlain) GetStorage() map[string]interface{} {
	return mh.extraStorage
}

func (mh *MHTextPlain) GetLineNumber() int {
	return mh.ln
}

func (mh *MHTextPlain) GetHeader() Header {
	return mh.h
}

func (mh *MHTextPlain) GetWrites() []int {
	return mh.writes
}

func (mh *MHTextPlain) GetBuffer() *bytes.Buffer {
	return mh.b
}

// SetHeaderItem modifies a header line in the MediaHandler's header
func (mh *MHTextPlain) SetHeaderItem(key, value string) {
	if h, ok := mh.h.(mail.Header); ok {
		textproto.MIMEHeader(h).Set(key, value)
	}
}

// GetAddressees returns a list of Address objects for all recipients.
// Note the extra "e" in the name.
func (mh *MHTextPlain) GetAddressees() []*mail.Address {
	return mh.addrs
}

// maybeDecodeBase64 wraps a reader in a base64 decoder if necessary.
//
// Note to self: investigate why this was present at some point but got lost.
func maybeDecodeBase64(h Header, b io.Reader) io.Reader {
	const cte = "Content-Transfer-Encoding"
	if ! strings.EqualFold(h.Get(cte), "base64") {
		return b
	}
	// This is what mime.bEncode uses, so it must be right (right?)
	return base64.NewDecoder(base64.StdEncoding, b)
}

// maybeBecomeQP possibly converts body reader into a quoted printable one.
// See src/mime/multipart.go newPart(mr *Reader, rawPart bool) (*Part, error)
// from which this was lifted.
func maybeBecomeQP(h Header, b io.Reader) io.Reader {
	const cte = "Content-Transfer-Encoding"
	if ! strings.EqualFold(h.Get(cte), "quoted-printable") {
		return maybeDecodeBase64(h, b)
	}
	if delable, ok := h.(interface{Del(string)}); ok {
		delable.Del(cte)
	}
	return quotedprintable.NewReader(b)
}

// filterQuotes drops lines that begin with >
func (mh *MHTextPlain) filterQuotes(line []byte) []byte {
	if line[0] == '>' {
		return []byte{}
	}
	return line
}

func (mh *MHTextPlain) filterWrites(line []byte) []byte {
	return filter.FilterWrites(mh, line)
}

// Handle prints lines, skipping some based on crude heuristics
func (mh *MHTextPlain) Handle() error {
	scanner := bufio.NewScanner(mh.f)
Lines:
	for scanner.Scan() {
		mh.ln += 1
		line := scanner.Bytes()
		for _, filter := range mh.filters {
			line = filter(line)
			if len(line) == 0 {
				continue Lines
			}
		}
		for _, filter := range mh.extraFilters {
			line = filter(mh, line)
			if len(line) == 0 {
				continue Lines
			}
		}
		if mh.b.Len() != 0 {
			if err := mh.b.WriteByte(' '); err != nil {
				return err
			}
		}
		mh.writes = append(mh.writes, len(line))
		_, err := mh.b.Write(line)
		if err != nil {
			return err
		}
	}
	if err := scanner.Err(); err != nil {
		return err
	}
	return nil
}

func getAddresses(h Header) ([]*mail.Address, error) {
	out := []*mail.Address{}
	for _, field := range []string{"to", "cc"} {
		emails, err := h.AddressList(field)
		if err != nil {
			if err == mail.ErrHeaderNotPresent {
				continue
			}
			return nil, err
		}
		out = append(out, emails...)
	}
	return out, nil
}

func CreateTextPlain(
	h Header, f io.Reader, buf *bytes.Buffer, filters... MediaFilter,
) (MediaHandler, error) {
	f = maybeBecomeQP(h, f)
	addrs, err := getAddresses(h)
	if err != nil {
		return nil, err
	}
	mh := &MHTextPlain{
		h, f, buf, 0, addrs, make([]func([]byte)[]byte, 0),
		make([]MediaFilter, 0), make(map[string]interface{}), []int{},
	}
	mh.filters = append(
		mh.filters, bytes.TrimSpace, mh.filterQuotes, mh.filterWrites,
	)
	mh.extraFilters = append(mh.extraFilters, filters...)
	return mh, nil
}
