// Pakcage multimail contains some helpers for dealing with git-multimail.
//
// See https://github.com/git-multimail/git-multimail. The headers are mostly
// compatible with the ones in post-receive-email.

package multimail

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"net/mail"
	"regexp"
)

var NotOursError = errors.New("Not ours")


func RepoMatch(h mail.Header, r *regexp.Regexp) bool {
	v, ok := h["X-Git-Repo"]
	if !ok {
		return false
	}
	return r.MatchString(v[0])
}

func RefMatch(h mail.Header, r *regexp.Regexp) bool {
	v, ok := h["X-Git-Refname"]
	if !ok {
		return false
	}
	return r.MatchString(v[0])
}

func RefTypeMatch(h mail.Header, r *regexp.Regexp) bool {
	v, ok := h["X-Git-Reftype"]
	if !ok {
		return false
	}
	return r.MatchString(v[0])
}

func RepoMatchString(h mail.Header, s string) bool {
	return RepoMatch(h, regexp.MustCompile(s))
}

func RefMatchString(h mail.Header, s string) bool {
	return RefMatch(h, regexp.MustCompile(s))
}

func RefTypeMatchString(h mail.Header, s string) bool {
	return RefTypeMatch(h, regexp.MustCompile(s))
}

func IsNewBranch(h mail.Header) bool {
	if !RefTypeMatchString(h, "branch") {
		return false
	}
	v, ok := h["X-Git-Oldrev"]
	if !ok {
		return false
	}
	return v[0] == "0000000000000000000000000000000000000000"
}

func IsDeletion(h mail.Header) bool {
	if !RefTypeMatchString(h, "branch") {
		return false
	}
	v, ok := h["X-Git-Newrev"]
	if !ok {
		return false
	}
	return v[0] == "0000000000000000000000000000000000000000"
}

func scrapeBranch(scanner *bufio.Scanner) (string, error) {
	for i := 0; i < 2 && scanner.Scan(); i++ {
		line := scanner.Bytes()
		if !bytes.HasPrefix(line, []byte("branch: ")) {
			continue
		}
		rest := line[8:]
		return string(rest), nil
	}
	return "", fmt.Errorf("Couldn't find branch")
}

var patCommitSHA = regexp.MustCompile(`^commit ([[:xdigit:]]{40})$`)

func ScrapeCommitSHA(scanner *bufio.Scanner) (string, error) {
	for i := 0; i < 2 && scanner.Scan(); i++ {
		line := scanner.Bytes()
		m := patCommitSHA.FindSubmatch(line)
		if m != nil {
			return string(m[1]), nil
		}
	}
	return "", fmt.Errorf("Couldn't find SHA")
}

var patAuthor = regexp.MustCompile(`^Author: (.+) <([^>]+)>$`)

func ScrapeAuthor(scanner *bufio.Scanner) ([]string, error) {
	lines := []string{}
	for i := 0; i < 3 && scanner.Scan(); i++ {
		line := scanner.Bytes()
		m := patAuthor.FindSubmatch(line)
		if m != nil {
			return []string{string(m[1]), string(m[2])}, nil
		}
		lines = append(lines, string(line))
	}
	return nil, fmt.Errorf("Couldn't find author Author: %v", lines)
}

var patCommitter = regexp.MustCompile(`^Commit: (.+) <([^>]+)>$`)

func ScrapeCommitter(scanner *bufio.Scanner) ([]string, error) {
	lines := []string{}
	for i := 0; i < 4 && scanner.Scan(); i++ {
		line := scanner.Bytes()
		m := patCommitter.FindSubmatch(line)
		if m != nil {
			return []string{string(m[1]), string(m[2])}, nil
		}
		lines = append(lines, string(line))
	}
	return nil, fmt.Errorf("Couldn't find author Author: %v", lines)
}

func ScrapeBranchMatchString(
	scanner *bufio.Scanner, r *regexp.Regexp,
) (string, error) {
	s, err := scrapeBranch(scanner)
	if err != nil {
		return "", err
	}
	if !r.MatchString(s) {
		return "", NotOursError
	}
	return s, nil
}

func ScrapeBranchMatch(scanner *bufio.Scanner, r *regexp.Regexp) error {
	_, err := ScrapeBranchMatchString(scanner, r)
	if err != nil {
		return err
	}
	return nil
}

var patMsgSubjectTop = regexp.MustCompile(`^[^\s]+ pushed a change to branch `)
var patMsgSubject = regexp.MustCompile(`^\s+(was|at|new)\s+[[:xdigit:]]+\s+(.+)$`)

func scrapeMsgSubject(scanner *bufio.Scanner) ([]string, error) {
	for i := 0; i < 2 && scanner.Scan(); i++ {
		line := scanner.Bytes()
		if len(line) == 0 {
			continue
		}
		if m := patMsgSubjectTop.Match(line); m {
			i--
			continue
		}
		m := patMsgSubject.FindSubmatch(line)
		if m != nil {
			return []string{string(m[1]), string(m[2])}, nil
		}
	}
	return nil, fmt.Errorf("Couldn't find deletion or new-branch msg subject")
}

func ScrapeMsgSubject(scanner *bufio.Scanner) (string, error) {
	ms, err := scrapeMsgSubject(scanner)
	if err != nil {
		return "", err
	}
	return ms[1], nil
}

func ScrapeDeletionMsgSubject(scanner *bufio.Scanner) (string, error) {
	ms, err := scrapeMsgSubject(scanner)
	if err != nil {
		return "", err
	}
	if ms[0] != "was" {
		return "", fmt.Errorf("Couldn't find msg subject for deletion: %v", ms)
	}
	return ms[1], nil
}

func ScrapeNewBranchMsgSubject(scanner *bufio.Scanner) (string, error) {
	ms, err := scrapeMsgSubject(scanner)
	if err != nil {
		return "", err
	}
	if ms[0] != "at" {
		return "", fmt.Errorf("Couldn't find msg subject for creation: %v", ms)
	}
	return ms[1], nil
}

var maxSubject = 5

func ScrapeSubject(scanner *bufio.Scanner) (string, error) {
	i := 0
	for ; scanner.Scan() && len(scanner.Bytes()) > 0 && i < maxSubject; i++ {
	}
	if i == maxSubject {
		return "", fmt.Errorf("Subject not found")
	}
	scanner.Scan()
	return string(bytes.TrimSpace(scanner.Bytes())), nil
}
