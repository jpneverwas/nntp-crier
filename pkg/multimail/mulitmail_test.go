package multimail

import (
	"bufio"
	"net/mail"
	"os"
	"testing"
)

func TestRepoMatchString(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_create.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	if !RepoMatchString(msg.Header, "emacs") {
		t.Fatal("Wanted emacs, got something else")
	}
}

func TestIsNewBranch(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_create.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	if !IsNewBranch(msg.Header) {
		t.Fatal("Expected new branch")
	}
	if IsDeletion(msg.Header) {
		t.Fatal("Unexpected deletion")
	}
}

func TestIsDeleteion(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_delete.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	if !IsDeletion(msg.Header) {
		t.Fatal("Expected rev")
	}
	if IsNewBranch(msg.Header) {
		t.Fatal("Unexpected rev")
	}
}

func TestScrapeBranch(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_change.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	scanner := bufio.NewScanner(msg.Body)
	s, err := scrapeBranch(scanner)
	if err != nil {
		t.Fatal(err)
	}
	if s != "feature/completions-customs" {
		t.Fatalf("Expected emacs, got %s", s)
	}
}

func TestScrapeCommitSHA(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_change.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	scanner := bufio.NewScanner(msg.Body)
	s, err := ScrapeCommitSHA(scanner)
	if err != nil {
		t.Fatal(err)
	}
	if s != "1641b5c04c383b5f53298d70776e3c18577b6f30" {
		t.Fatalf("Expected emacs, got %s", s)
	}
}

func TestScrapeAuthor(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_change.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	scanner := bufio.NewScanner(msg.Body)
	ss, err := ScrapeAuthor(scanner)
	if err != nil {
		t.Fatal(err)
	}
	if ss[0] != "Jimmy Aguilar Mena" {
		t.Fatalf("Expected emacs, got %s", ss)
	}
	if ss[1] != "spacibba@aol.com" {
		t.Fatalf("Expected emacs, got %s", ss)
	}
}

func TestScrapeCommitter(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_change.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	scanner := bufio.NewScanner(msg.Body)
	ss, err := ScrapeCommitter(scanner)
	if err != nil {
		t.Fatal(err)
	}
	if ss[0] != "Jimmy Aguilar Mena" {
		t.Fatalf("Expected emacs, got %s", ss)
	}
	if ss[1] != "spacibba@aol.com" {
		t.Fatalf("Expected emacs, got %s", ss)
	}
}

func TestScrapeSubject(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_change.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	scanner := bufio.NewScanner(msg.Body)
	s, err := ScrapeSubject(scanner)
	if err != nil {
		t.Fatal(err)
	}
	if s != "Set cursor-face-highlight-nonselected-window in completions." {
		t.Fatalf("Expected subject, got %s", s)
	}
}

func TestScrapeDeletionMsgSubject(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_delete.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	scanner := bufio.NewScanner(msg.Body)
	s, err := ScrapeDeletionMsgSubject(scanner)
	if err != nil {
		t.Fatal(err)
	}
	if s != "Small fixes for new completions features" {
		t.Fatalf("Wanted 'Small fixes...', got %s", s)
	}
}

func TestScrapeNewBranchMsgSubject(t *testing.T) {
	f, err := os.Open("testdata/diff_cmpcust_create.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	scanner := bufio.NewScanner(msg.Body)
	s, err := ScrapeNewBranchMsgSubject(scanner)
	if err != nil {
		t.Fatal(err)
	}
	if s != "Add completion-auto-select second-tab value." {
		t.Fatalf("Wanted 'Small fixes...', got %s", s)
	}
}
