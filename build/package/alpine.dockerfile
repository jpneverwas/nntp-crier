FROM docker.io/library/golang:1.18-alpine AS build

ADD . /go/src/gitlab.com/jpneverwas/nntp-crier
WORKDIR /go/src/gitlab.com/jpneverwas/nntp-crier

RUN apk add --no-cache git build-base

RUN go get -t -v ./... && \
	CGO_ENABLED=0 go build -a -o /go/bin/ ./... && \
	ls -alh /go/bin && \
	/go/bin/crier -help

RUN CGO_ENABLED=0 go install \
	gitlab.com/jpneverwas/urokotori/cmd/rearview@latest

FROM alpine:latest
COPY --from=build /go/bin/* /usr/local/bin/
COPY LICENSE /crier/

RUN crier -help

ENV CRIER_DATABASE=/crier/crier.db
ENV CRIER_SERVER=news.eternal-september.org:119
ENV CRIER_HANDLER=noop

CMD ["crier", "eternal-september.test=."]
# vim:ft=dockerfile
