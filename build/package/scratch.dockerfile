FROM docker.io/library/golang:1.18 AS build

ADD . /go/src/gitlab.com/jpneverwas/nntp-crier
WORKDIR /go/src/gitlab.com/jpneverwas/nntp-crier

RUN go get -t -v ./... && \
	CGO_ENABLED=0 go build -a -o /go/bin/ ./... && \
	ls -alh /go/bin/ && \
	/go/bin/crier -help

RUN CGO_ENABLED=0 go install \
	gitlab.com/jpneverwas/urokotori/cmd/rearview@v0.1.0

FROM scratch
COPY --from=build /go/bin/* /bin/
COPY --from=build /etc/ssl/certs/* /etc/ssl/certs/
COPY LICENSE /crier/

ENV CRIER_DATABASE=/crier/crier.db
ENV CRIER_SERVER=news.eternal-september.org:119
ENV CRIER_HANDLER=noop
ENV PATH=/bin

RUN ["/bin/crier", "-help"]

CMD ["crier", "eternal-september.test=."]
# vim:ft=dockerfile
