#!/bin/sh

# This is an example showing how to send something to an irccat HTTP endpoint.
# It requires coreutils (or busybox) and curl.
#
# Git calls this with three space-delimited fields on stdin.  Something like:
#
#   deadbeef..1 deadbeef..2 refs/heads/master
#
# where (1) is usually the full SHA1 hash of the commit before the push and (2)
# is the one just pushed.  See githooks(5).
#
# In these hook scripts, options like these are typically stored in a git
# repo's config under [hooks].  See contrib/hooks/post-receive-email by Andy
# Parkins in git.git for an example of this as part of a proper hook script
# (which this example is not).

url="https://git.savannah.gnu.org/cgit/poke.git/commit/?id="
channel='#poke'
endpoint=http://irc:8045/send
auth="Authorization: Bearer changeme"


create_body() {
    shortref=$(basename "$refname")
    name=$(git log '--format=%an' -1 "$1")
    date=$(git log '--format=%ah' -1 "$1")
    subj=$(git log '--format=%s' -1 "$1")
    body=$(git log '--format=%b' -1 "$1")
    body=$(printf %s "$body" | paste -s -d' ' | sed 's/[ 	]\+/ /g')
    link=$(printf '%s%.8s' "$url" "$1")
    [ -n "$body" ] && body="| $body"
    out=$(
        printf '[%s] %%BOLD%s%%NORMAL %s %s %s %s' \
            "$shortref" "$name" "$date" "$link" "$subj" "$body"
    )
    lenout=$(printf %s "$out" | wc -c)
    if [ "$lenout" -gt 369 ]; then
        printf %s "$out" | head -c 366
        printf ...
    else
        printf %s "$out"
    fi
}

main() {
    read -r _ new refname
    final=$(create_body "$new" "$refname")
    printf '%s %s\n' "$channel" "$final" | \
        curl --data-urlencode @- -H "$auth" "$endpoint"
}

if [ $# -eq 0 ]; then
    main
else
    "$@"
fi
