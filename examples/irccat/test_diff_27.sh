#!/bin/sh
fakebin=$(mktemp -d)
touch "$fakebin/id2mm"
chmod 700 "$fakebin/id2mm"

set -- "gmane.emacs.diffs" \
    "Subject: emacs-27 b77f6af: ERC right stamps: also use latest buffer's window's width (Bug#44140)" \
    "From: bandali@gnu.org (Amin Bandali)" \
    "Date: Fri,  6 Aug 2021 01:16:03 -0400 (EDT)" \
    "Message-Id: <20210806051603.9DA8920BD3@vcs0.savannah.gnu.org>" \
    "References: <20210806051601.7440.90873@vcs0.savannah.gnu.org>" \
    ":bytes 8522" \
    ":lines 108" \
    "Xref:full Xref: news.gmane.io gmane.emacs.diffs:165540"

output=$(mktemp)

DIFF_STAT_PAT='/lisp/emacs-lisp\|/doc/lispref/files\.texi' \
    PATH=$fakebin:$PATH \
    IRCCAT_DEBUG=1 \
    IRCCAT_TARGET='#chan' \
    IRCCAT_LISTEN_HOST=localhost \
    IRCCAT_LISTEN_PORT=12345 sh ./standalone.sh "$@" \
    < ./testdata/diff_27.email > "$output" 2>&1
ec=$?
result=$(cat "$output")

printf "Checking: no match case... "
if [ "$ec" -ne 31 ]; then
    printf "Failed: wrong exit code: %d\n%s\n" "$ec" "$result"
    exit 1
fi
if ! printf %s "$result" | grep "not ours, bailing" >/dev/null; then
    printf "Failed: wrong output:\n%s\n" "$result"
    exit 1
fi
echo "Passed."

PATH=$fakebin:$PATH \
    IRCCAT_DEBUG=1 \
    IRCCAT_TARGET='#chan' \
    IRCCAT_LISTEN_HOST=localhost \
    IRCCAT_LISTEN_PORT=12345 sh ./standalone.sh "$@" \
    < ./testdata/diff_27.email > "$output" 2>&1
ec=$?
result=$(cat "$output")

expect="#chan ~ %BOLDOlivier Certner%NORMAL ~ 6 Aug 2021 01:16:03 -0400 (EDT)"
expect="$expect ~ ERC right stamps: also use latest buffer's window's width (Bug#44140)"
expect="$expect ● https://git.savannah.gnu.org/cgit/emacs.git/commit/?id="
expect="${expect}b77f6af24e9193a4f70622cda6d641c58e885c22"

printf "Checking: match... "
if [ "$ec" -ne 31 ]; then
    printf "Failed: wrong exit code: %d\n%s\n" "$ec" "$result"
    exit 1
fi
if ! printf %s "$result" | grep -F "$expect" >/dev/null; then
    printf "Failed: wrong output:\n%s\n" "$result"
    exit 1
fi
echo "Passed."

rm -f "$output"
rm -rf "${fakebin:-/tmp/fake}"
