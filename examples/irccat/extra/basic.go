package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
)

const AuthorInfo = `Your name here (if you rewrite me using ERC and Gnus)!
Control channel: ircs://irc.libera.chat:6697/##erc-v3-ci`
const versionInfo = "\x02The ERC News Bot\x02 version 0.1" +
	" ● ERC is a powerful, modular, extensible IRC client for Emacs." +
	" More info at: emacs-erc@gnu.org" +
	" https://www.gnu.org/software/emacs/erc.html" +
	" ircs://irc.libera.chat:6697/#erc"
const sourceInfo = `GPLv3+: http://gnu.org/licenses/gpl.html
 Front: https://github.com/irccloud/irccat
  Back: https://gitlab.com/jpneverwas/nntp-crier
   Please rewrite me in Emacs Lisp {⚙_⚙}`

var commands = map[string]func(envvars) (string, error){}

type envvars struct {
	Nick      string
	User      string
	Host      string
	Channel   string
	Recipient string
	Command   string
	Args      []string
	Raw       string
}

func doVersion(_ envvars) (string, error) {
	return versionInfo, nil
}

func doAuthor(_ envvars) (string, error) {
	return AuthorInfo, nil
}

func doSource(_ envvars) (string, error) {
	return sourceInfo, nil
}

func doHelp(_ envvars) (string, error) {
	return "Available commands: " + getHelpString(), nil
}

func doDebug(vars envvars) (string, error) {
	out, err := json.Marshal(vars)
	if err != nil {
		return "", err
	}
	return string(out), nil
}

func getHelpString() string {
	var cmds []string
	for k := range commands {
		cmds = append(cmds, k)
	}
	return strings.Join(cmds, ", ")
}

func main() {
	commands["version"] = doVersion
	commands["debug"] = doDebug
	commands["author"] = doAuthor
	commands["source"] = doSource
	commands["help"] = doHelp

	vars := envvars{
		os.Getenv("IRCCAT_NICK"),
		os.Getenv("IRCCAT_USER"),
		os.Getenv("IRCCAT_HOST"),
		os.Getenv("IRCCAT_CHANNEL"),
		os.Getenv("IRCCAT_RESPOND_TO"),
		os.Getenv("IRCCAT_COMMAND"),
		strings.Fields(os.Getenv("IRCCAT_ARGS")),
		os.Getenv("IRCCAT_RAW"),
	}
	cmd, ok := commands[vars.Command]
	if !ok {
		fmt.Printf("No understandy: %q\n", vars.Command)
		fmt.Println("Understand:", getHelpString())
		os.Exit(0)
	}
	out, err := cmd(vars)
	if err != nil {
		fmt.Println(fmt.Errorf("Bad news: %q", err))
		os.Exit(1)
	}
	fmt.Println(out)
}
