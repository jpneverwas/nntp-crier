#!/bin/sh
fakebin=$(mktemp -d)
cat > "$fakebin/id2mm" << EOF
exit 42
EOF
chmod 700 "$fakebin/id2mm"

set -- "gmane.emacs.devel" \
    "Subject: Re: Highlight current line when using gud" \
    "From: Eli Zaretskii <eliz@gnu.org>" \
    "Date: Sun, 04 Jul 2021 07:30:29 +0300" \
    "Message-Id: <838s2mitui.fsf@gnu.org>" \
    "References: <46933061-DFBC-406C-8489-F3DC7EE770BA@kuaishou.com> <CAHO21CJQ+qqcJ7SYmEHbmBdEcNSvuRooFvp4vxwDOgp2qj7iFw@mail.gmail.com> <87sg0z8t5t.fsf@gnus.org> <6F39630A-1FF8-45DE-9E3B-D9E8BE906BD7@kuaishou.com> <83o8bnmqyo.fsf@gnu.org> <62662FDA-8D67-459B-8F03-662010FB96EB@kuaishou.com> <87y2aq1dhm.fsf@gnus.org> <87zgv5dh80.fsf@gmail.com> <87eechvv1p.fsf@gnus.org> <83eechj65u.fsf@gnu.org> <874kdbhr8b.fsf@gmail.com>" \
    ":bytes 4052" \
    ":lines 20" \
    "Xref:full: Xref: news.gmane.io gmane.emacs.devel:271120"

output=$(mktemp)
PATH=$fakebin:$PATH \
    IRCCAT_DEBUG=1 \
    IRCCAT_TARGET='#chan' \
    IRCCAT_LISTEN_HOST=localhost \
    IRCCAT_LISTEN_PORT=12345 sh ./standalone.sh "$@" \
    < ./testdata/base.email > "$output" 2>&1
ec=$?
result=$(cat "$output")

printf "Checking: service exit code propagation... "
if [ "$ec" -ne 42 ]; then
    printf "Failed: wrong exit code: %d\n%s\n" "$ec" "$result"
    exit 1
fi
if ! printf %s "$result" | grep "service returned" >/dev/null; then
    printf "Failed: wrong output:\n%s\n" "$result"
    exit 1
fi
echo "Passed."

echo 'printf %s http://quimby.gnus.org/pipermail/gmane-test/12345' > "$fakebin/id2mm"

PATH=$fakebin:$PATH \
    IRCCAT_DEBUG=1 \
    IRCCAT_TARGET='#chan' \
    IRCCAT_LISTEN_HOST=localhost \
    IRCCAT_LISTEN_PORT=12345 sh ./standalone.sh "$@" \
    < ./testdata/base.email > "$output" 2>&1
ec=$?
result=$(cat "$output")

expect="#chan ~ %BOLDEli Zaretskii%NORMAL ~ Sun, 04 Jul 2021 07:30:29 +0300 \
~ http://quimby.gnus.org/pipermail/gmane-test/12345 ● Re: Highlight current \
line when using gud | Thanks in advance. Indeed, and the manual is correct. \
But you still need mem regardless, I think Thanks! nother line Last line. \
This should be truncated with elipses right about here. \
Unless something's g..."

printf "Checking: match... "
if [ "$ec" -ne 0 ]; then
    printf "Failed: wrong exit code: %d\n%s\n" "$ec" "$result"
    exit 1
fi
if ! printf %s "$result" | grep -F "$expect" >/dev/null; then
    printf "Failed: wrong output:\n%s\n" "$result"
    exit 1
fi
echo "Passed."

rm -f "$output"
rm -rf "${fakebin:-/tmp/fake}"
