#!/bin/sh
fakebin=$(mktemp -d)
touch "$fakebin/id2mm"
chmod 700 "$fakebin/id2mm"

set -- "gmane.emacs.diffs" \
    "Subject: master 4bfdf8c: * doc/lispref/files.texi (Magic File Names): Add make-lock-file-name." \
    "From: Michael.Albinus@gmx.de (Michael Albinus)" \
    "Date: Thu,  8 Jul 2021 15:14:37 -0400 (EDT)" \
    "Message-Id: <20210708191437.ADD1920B72@vcs0.savannah.gnu.org>" \
    "References: <20210708191435.17802.11341@vcs0.savannah.gnu.org>" \
    ":bytes 4719" \
    ":lines 41" \
    "Xref:full Xref: news.gmane.io gmane.emacs.diffs:165064"

output=$(mktemp)
PATH=$fakebin:$PATH \
    IRCCAT_DEBUG=1 \
    IRCCAT_TARGET='#chan' \
    IRCCAT_LISTEN_HOST=localhost \
    IRCCAT_LISTEN_PORT=12345 sh ./standalone.sh "$@" \
    < ./testdata/diff.email > "$output" 2>&1
ec=$?
result=$(cat "$output")

printf "Checking: no match case... "
if [ "$ec" -ne 31 ]; then
    printf "Failed: wrong exit code: %d\n%s\n" "$ec" "$result"
    exit 1
fi
if ! printf %s "$result" | grep "not ours, bailing" >/dev/null; then
    printf "Failed: wrong output:\n%s\n" "$result"
    exit 1
fi
echo "Passed."

DIFF_STAT_PAT='/lisp/erc\|/doc/lispref/files\.texi' \
    PATH=$fakebin:$PATH \
    IRCCAT_DEBUG=1 \
    IRCCAT_TARGET='#chan' \
    IRCCAT_LISTEN_HOST=localhost \
    IRCCAT_LISTEN_PORT=12345 sh ./standalone.sh "$@" \
    < ./testdata/diff.email > "$output" 2>&1
ec=$?
result=$(cat "$output")

expect="#chan ~ %BOLDMichael Albinus%NORMAL ~ 8 Jul 2021 15:14:37 -0400 (EDT)"
expect="$expect ~ * doc/lispref/files.texi (Magic File Names): Add make-lock-file-name."
expect="$expect ● https://git.savannah.gnu.org/cgit/emacs.git/commit/?id="
expect="${expect}4bfdf8c78ee8d4f85f0d226006c3cc891bee837f"

printf "Checking: match... "
if [ "$ec" -ne 31 ]; then
    printf "Failed: wrong exit code: %d\n%s\n" "$ec" "$result"
    exit 1
fi
if ! printf %s "$result" | grep -F "$expect" >/dev/null; then
    printf "Failed: wrong output:\n%s\n" "$result"
    exit 1
fi
echo "Passed."

rm -f "$output"
rm -rf "${fakebin:-/tmp/fake}"
