#!/bin/sh

# Standalone means runs in alpine container without apk add
set -e

command -v id2mm >/dev/null

[ -n "$IRCCAT_TARGET" ]
[ -n "$IRCCAT_LISTEN_HOST" ]
[ -n "$IRCCAT_LISTEN_PORT" ]

list_name=

if [ "$1" = gmane.emacs.bugs ]; then
    list_name=bug-gnu-emacs
elif [ "$1" = gmane.emacs.help ]; then
    list_name=help-gnu-emacs
elif [ "$1" = gmane.emacs.devel ]; then
    list_name=emacs-devel
elif [ "$1" = gmane.emacs.erc.general ]; then
    list_name=emacs-erc
elif [ "$1" = gmane.emacs.diffs ]; then
    list_name=emacs-diffs
fi

if [ -z $list_name ]; then
    echo "Bad list name"
    printf '%s\t\n' "$@"
    exit 1
fi >&2

EXIT_CODE=0
if [ -z "$DIFF_STAT_PAT" ]; then
    DIFF_STAT_PAT='/\?lisp/erc\|doc/misc/erc\.texi'
fi

process_diff() {
    read -r line
    if ! printf %s "$line" | grep 'branch: \(master\|emacs-[[:digit:]]\)' >/dev/null; then
        return
    fi
    read -r line
    commit=$(printf %s "$line" | cut -f2 -d' ')
    part_url="https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=$commit"
    read -r line
    part_from=$(printf %s "$line" | sed -e 's/^Author: //' -e 's/ <[^>]\+>$//')
    read -r line; read -r line; read -r line
    if ! cat | grep "$DIFF_STAT_PAT" >/dev/null; then
        echo "Handler: not ours, bailing" >&2
        return 1
    fi
    part_subject=$(printf %s "$line" | sed -e 's/^ \+//')
    part_date=$(printf %s "$4" | sed 's/^Date: [^,]\+,[[:space:]]\+//')
    final="~ %BOLD$part_from%NORMAL ~ $part_date ~ $part_subject ● $part_url"
}

process_default() {
    part_subject=$(printf %s "$2" | sed 's/^Subject: //')
    part_from=$(
        printf %s "$3" | sed -e 's/^From: //' -e 's/ <[^>]\+>$//' -e 's/ via .\+$//'
    )
    part_date=$(printf %s "$4" | sed 's/^Date: //')
    part_id=$(printf %s "$5" | sed 's/^Message-Id: //')
    part_body=$(
        cat | awk -v ORS=' ' '
        NR < 5 && /.+ wrote:$|.+ writes:$/ {next;}
        /^>/ {next;}
        /^[\t ]+>/ {next;}
        /^[[:space:]]*$/ {next;}
        1 {sub(/\r/, ""); print}
        '
    )

    sub_out=$(mktemp)
    ec=$(id2mm -list "$list_name" -id "$part_id" > "$sub_out"; echo $?)
    part_url=$(cat "$sub_out")
    rm -f "$sub_out"
    if ! [ "$ec" = 0 ]; then
        echo "Handler: service returned $ec" >&2
        exit "$ec"
    fi

    if ! part_url=$(id2mm -list "$list_name" -id "$part_id") ec=$?; then
        exit $ec
    fi
    final="~ %BOLD$part_from%NORMAL ~ $part_date ~ $part_url ● $part_subject | $part_body"
}

(while read -r line && [ -n "$line" ]; do :; done)

if [ "$list_name" = "emacs-diffs" ]; then
    EXIT_CODE=31
    process_diff "$@" || exit 31
else
    process_default "$@"
fi

# Trim final
if [ "$(printf %s "$final" | wc -c)" -ge 369 ]; then
    final=$(printf %s "$final" | head -c 366; printf '...')
fi

if [ -n "$IRCCAT_DEBUG" ]; then
    printf '%s %s\n' "$IRCCAT_TARGET" "$final"
else
    printf '%s %s\n' "$IRCCAT_TARGET" "$final" | \
        nc "$IRCCAT_LISTEN_HOST" "$IRCCAT_LISTEN_PORT"
fi

exit "$EXIT_CODE"
