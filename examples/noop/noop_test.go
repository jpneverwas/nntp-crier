package main

import (
	"bytes"
	"os"
	"os/exec"
	"testing"
	"time"

	helpers "gitlab.com/jpneverwas/nntp-crier/internal/testing"
)

const expect = `
Args:
	one
	two
Stdin:
Hello,
Some words...
Bye
Done!
`

func TestMain(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "Main" {
		main()
		os.Exit(0)
	}
	cmd := exec.Command(os.Args[0], "-test.run=TestMain", "one", "two")
	cmd.Stdin = bytes.NewBuffer([]byte("Hello,\nSome words...\nBye\n"))
	cmd.Env = append(
		os.Environ(),
		"GO_WANT_HELPER_PROCESS=Main",
	)
	output, err := cmd.Output()
	if err != nil {
		if ee, ok := err.(*exec.ExitError); ok {
			t.Fatalf("Failed to spawn child process: %v, %q", err, ee.Stderr)
		}
		t.Fatalf("Failed to spawn child process: %v", err)
	}
	now := time.Now().Format("Mon Jan 2 15:04:05 -0700 MST 2006")
	if err := helpers.Compare([]byte(now + expect), output); err != nil {
		t.Fatal(err)
	}
}
