package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"time"
)

func main() {
	fmt.Println(time.Now().Format("Mon Jan 2 15:04:05 -0700 MST 2006"))
	fmt.Println("Args:")
	flag.Parse()
	for _, arg := range flag.Args() {
		fmt.Printf("\t%s\n", arg)
	}
	fmt.Println("Stdin:")
	_, err := bufio.NewReader(os.Stdin).WriteTo(os.Stdout)
	if err != nil {
		panic(err)
	}
	fmt.Println("Done!")
}
