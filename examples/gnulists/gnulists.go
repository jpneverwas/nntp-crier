// Command gnulists is an example of a crier handler
//
// See ../irccat/standalone.sh for the same thing in (poor) shell script.
//
// This supports sending output to an irccat listener:
//
//  IRCCAT_TARGET=#chan
//  IRCCAT_LISTEN_HOST=irccat.my.vps
//  IRCCAT_LISTEN_PORT=12345
//
// For target, you can also do:
//
//  IRCCAT_TARGET=gmane.foo=#chan,gmane=bar=#spam,default=#test
//
// Where "default" is the fallback channel.
//
// To print output to stdout, set IRCCAT_DEBUG=1

package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/mail"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jpneverwas/nntp-crier/internal/transpo"
	"gitlab.com/jpneverwas/nntp-crier/pkg/filter"
)

const puntCode = 31

var groups2lists = map[string]string{
	"gmane.emacs.bugs":        "bug-gnu-emacs",
	"gmane.emacs.help":        "help-gnu-emacs",
	"gmane.emacs.sources":     "gnu-emacs-sources",
	"gmane.emacs.devel":       "emacs-devel",
	"gmane.emacs.erc.general": "emacs-erc",
	"gmane.emacs.diffs":       "emacs-diffs",
	"gmane.emacs.buildstatus": "emacs-buildstatus",
}

var logger = log.Default()

var msgIdPat = regexp.MustCompile(`^Message-I[Dd]: (.+)$`)

func maybeFetchUsingFallback(list, msgId string) ([]byte, error) {
	cmd := exec.Command("rearview", "-list", list, "-id", msgId, "-max", "100")
	return cmd.Output()
}

type exitCodeDirect int

func (e exitCodeDirect) Error() string {
	return fmt.Sprintf("Force-exited: %d", e)
}

// checkHealth performs a health check on an http endpoint
// An HTTP status of 2xx passes. Anything else fails.
func checkHealth(endpoint string, maxwait string) error {
	if endpoint == "" {
		return nil
	}
	n, err := strconv.Atoi(maxwait)
	if err != nil {
		return err
	}
	if n <= 0 {
		return nil
	}
	for i := 1; i < n; i *= 2 {
		c := &http.Client{Timeout: 5 * time.Second}
		resp, err := c.Get(endpoint)
		if err != nil {
			if !strings.Contains(err.Error(), "connection refused") &&
				!strings.Contains(err.Error(), "EOF") &&
				!strings.Contains(err.Error(), "deadline exceeded") {
				return err
			}
			log.Println(err)
		} else {
			if resp.StatusCode >= 200 && resp.StatusCode < 300 {
				return nil
			}
			log.Println(resp.StatusCode, resp.Status)
		}
		time.Sleep(time.Duration(i) * time.Second)
	}
	return fmt.Errorf("Health check failed")
}

// fetchUrl makes a request by shelling out to id2mm
//
// That program takes a list from groups2lists and a Message-Id header line
// and returns a mailman URL or exits 42 if not found.
//
// As a fallback, it may try the external rearview command, if available.
func fetchUrl(list, rawMsgId string) ([]byte, error) {
	m := msgIdPat.FindStringSubmatch(rawMsgId)
	if m == nil {
		return nil, fmt.Errorf("Bad msgId: %s", rawMsgId)
	}
	msgId := m[1]
	if filter.GmanePat.MatchString(msgId) {
		return nil, exitCodeDirect(puntCode)
	}
	cmd := exec.Command("id2mm", "-list", list, "-id", msgId)
	output, origErr := cmd.Output()
	var origEC int
	if origErr != nil {
		if maybeExitError, ok := origErr.(*exec.ExitError); ok {
			origEC = maybeExitError.ProcessState.ExitCode()
		}
		if origEC == 1 {
			return nil, origErr
		}
		if _, err := exec.LookPath("rearview"); err == nil {
			fbOutput, err := maybeFetchUsingFallback(list, msgId)
			if err != nil {
				return nil, err
			}
			fbOutput = bytes.TrimSpace(fbOutput)
			if len(fbOutput) != 0 {
				return fbOutput, nil
			}
		}
	}
	return output, origErr
}

func maybeExitSpecial(err error) {
	var ec int
	switch e := err.(type) {
	case exitCodeDirect:
		ec = int(e)
		fmt.Fprintf(os.Stderr, "Handler: service returned %d\n%s", e, e.Error())
		goto done
	case *exec.ExitError:
		ec = e.ProcessState.ExitCode()
		fmt.Fprintf(os.Stderr, "Handler: service returned %d\n%s", ec, e.Stderr)
		goto done
	case interface{StatusCode()int}:
		if e.StatusCode() >= 400 {
			fmt.Fprintf(os.Stderr, "Handler: %s", e)
			ec = puntCode
			goto done
		}
		logger.Fatal(e)
	default:
		logger.Fatal(err)
	}
done:
	os.Exit(ec)
}

var sendOutput = transpo.CatOutput

func parseTarget(in, group string) (string, error) {
	if !strings.Contains(in, ",") {
		return in, nil
	}
	var fallback string
	for _, s := range strings.Split(in, ",") {
		kv := strings.SplitN(s, "=", 2)
		if group == kv[0] {
			return kv[1], nil
		}
		if kv[0] == "default" {
			fallback = kv[1]
		}
	}
	if fallback != "" {
		return fallback, nil
	}
	return "", fmt.Errorf("Couldn't parse target %s for group %s", in, group)
}

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) < 6 {
		logger.Fatalf("Missing args: %v", args)
	}
	group := args[0]
	list, ok := groups2lists[group]
	if !ok {
		logger.Fatalf("Unknown group: %v", group)
	}
	if err := checkHealth(
		os.Getenv("IRCCAT_HEALTHCHECK"),
		os.Getenv("IRCCAT_HEALTHCHECK_MAXSECS"),
	); err != nil {
		logger.Fatal(err)
	}
	var output []byte
	switch list {
	default:
		{
			url, err := fetchUrl(list, args[4])
			if err != nil {
				maybeExitSpecial(err)
			}
			cmd := exec.Command(
				"compose", "-max", "369", "-url", string(url), "-source", "-",
			)
			buf := bufio.NewReader(os.Stdin)
			cmd.Stdin = buf
			output, err = cmd.Output()
			if err != nil {
				maybeExitSpecial(err)
			}
		}
	case "emacs-diffs":
		{
			dsp := diffStatPat
			if maybeDsp := os.Getenv("DIFF_STAT_PAT"); maybeDsp != "" {
				dsp = regexp.MustCompile(maybeDsp)
			}
			msg, err := mail.ReadMessage(os.Stdin)
			if err != nil {
				logger.Fatal(err)
			}
			output, err = handleDiff(msg, args[3], dsp)
			if err != nil {
				if err == NotOursError {
					os.Exit(puntCode)
				}
				logger.Fatal(err)
			}
		}
	case "emacs-buildstatus":
		{
			bsp := buildStatusPat
			if mbs := os.Getenv("BUILD_STAT_PAT"); mbs != "" {
				bsp = regexp.MustCompile(mbs)
			}
			msg, err := mail.ReadMessage(os.Stdin)
			if err != nil {
				logger.Fatal(err)
			}
			output, err = handleBuildStatus(msg, args[3], bsp)
			if err != nil {
				if err == NotOursError {
					os.Exit(puntCode)
				}
				maybeExitSpecial(err)
			}
		}
	}
	if debug := os.Getenv("IRCCAT_DEBUG"); debug != "" {
		fmt.Print(string(output))
		os.Exit(0)
	}
	target, err := parseTarget(os.Getenv("IRCCAT_TARGET"), group)
	if err != nil {
		logger.Fatal(err)
	}
	host := os.Getenv("IRCCAT_LISTEN_HOST")
	port := os.Getenv("IRCCAT_LISTEN_PORT")
	// Allow empty host
	if target == "" || port == "" {
		logger.Fatalf("Missing env vars for output service")
	}
	if err := sendOutput(target, host+":"+port, output); err != nil {
		logger.Fatal(err)
	}
}
