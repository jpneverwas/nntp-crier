package main

import (
	"bufio"
	"fmt"
	"gitlab.com/jpneverwas/nntp-crier/pkg/multimail"
	"gitlab.com/jpneverwas/nntp-crier/pkg/filter"
	"gitlab.com/jpneverwas/nntp-crier/pkg/compose"
	"net/mail"
	"regexp"
)

const svUrl = "https://git.savannah.gnu.org/cgit/emacs.git/commit/?id="

var diffStatPat = regexp.MustCompile("/?lisp/erc|doc/misc/erc[.]texi|etc/ERC-NEWS")
var diffBranch = regexp.MustCompile(`(master|emacs-\d+)`)
var diffRef = regexp.MustCompile(`^refs/heads/(master|emacs-\d+)`)
var diffRefNewDel = regexp.MustCompile(`^refs/heads/(feature|fix)`)
var diffRepo = regexp.MustCompile("^emacs$")
var diffMsgSubject = regexp.MustCompile(`\b(erc|ERC)\b`)

var diffDate = regexp.MustCompile(`^Date: [^,]+,[[:space:]]+(.+)$`)

var NotOursError = multimail.NotOursError

func checkOurs(scanner *bufio.Scanner, pat *regexp.Regexp) bool {
	for scanner.Scan() {
		b := scanner.Bytes()
		if len(b) == 0 {
			return false
		}
		if pat.Match(b) {
			return true
		}
	}
	return false
}

func renderDate(s string) (string, error) {
	date, err := mail.ParseDate(s)
	if err != nil {
		return "", err
	}
	return date.Format("Mon, Jan 2 2006 15:04:05 -0700"), nil
}

func handleNewDel(
	msg *mail.Message, rawDate string, diffPat *regexp.Regexp,
) ([]byte, error) {
	if !multimail.RefMatch(msg.Header, diffRefNewDel) {
		return nil, NotOursError
	}
	scanner := bufio.NewScanner(msg.Body)
	msgsub, err := multimail.ScrapeMsgSubject(scanner)
	if err != nil {
		return nil, err
	}
	if !diffMsgSubject.MatchString(msgsub) {
		return nil, NotOursError
	}
	subject := msg.Header.Get("subject")
	ms := diffDate.FindStringSubmatch(rawDate)
	if ms == nil {
		return nil, fmt.Errorf("Unexpected Date: %v", rawDate)
	}
	date, err := renderDate(ms[1])
	if err != nil {
		return nil, err
	}
	filter.MessageFilterVia(msg.Header)
	from, err := msg.Header.AddressList("from")
	if err != nil {
		return nil, err
	}
	author := from[0].Name
	out := []byte(`~ %BOLD` + author + `%NORMAL ~ ` + date + ` ~ ` + subject)
	return out, nil
}

func handleChange(
	msg *mail.Message, rawDate string, diffPat *regexp.Regexp,
) ([]byte, error) {
	scanner := bufio.NewScanner(msg.Body)
	// Branch
	branch, err := multimail.ScrapeBranchMatchString(scanner, diffBranch)
	if err != nil {
		// Must be a direct or other type update
		return nil, NotOursError
	}
	// Commit SHA
	var url string
	if sha, err := multimail.ScrapeCommitSHA(scanner); err != nil {
		return nil, err
	} else {
		url = svUrl + sha
	}
	// Author
	authorInfo, err := multimail.ScrapeAuthor(scanner)
	if err != nil {
		return nil, err
	}
	author := authorInfo[0]
	// Subject
	subject, err := multimail.ScrapeSubject(scanner)
	if err != nil {
		return nil, err
	}
	// FF
	if !checkOurs(scanner, diffPat) {
		return nil, NotOursError
	}
	// Date
	ms := diffDate.FindStringSubmatch(rawDate)
	if ms == nil {
		return nil, fmt.Errorf("Unexpected Date: %v", rawDate)
	}
	date, err := renderDate(ms[1])
	if err != nil {
		return nil, err
	}
	out := []byte(
		"[" + branch + `] %BOLD` + author + `%NORMAL ~ ` + date + ` ~ ` +
			compose.ShortenRevision(url) + ` ● ` + subject,
	)
	return out, nil
}


// handleDiff attempts to compose a message from an emacs-diffs stat
func handleDiff(
	msg *mail.Message, rawDate string, diffPat *regexp.Regexp,
) ([]byte, error) {
	if !multimail.RepoMatch(msg.Header, diffRepo) {
		return nil, NotOursError
	}
	if multimail.IsDeletion(msg.Header) || multimail.IsNewBranch(msg.Header) {
		return handleNewDel( msg, rawDate, diffPat)
	}
	if !multimail.RefMatch(msg.Header, diffRef) {
		return nil, NotOursError
	}
	return handleChange(msg, rawDate, diffPat)
}
