package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/mail"
	"regexp"
	"strings"

	"gitlab.com/jpneverwas/nntp-crier/pkg/feed"
)

var patTed = regexp.MustCompile(`triggered by (Ted Zlatanov|Michael Albinus)`)
var patRaw = regexp.MustCompile(`^Job #[[:digit:]]+ [(] *(https://[^ ]+) *[)]`)
var buildStatusPat = regexp.MustCompile(`(test/)?lisp/erc\b`)
var customOptsPat = regexp.MustCompile(`misc/test-custom-opts\b`)
var customOptsSectionCookie = regexp.MustCompile(`^[[:digit:]]+ options tested`)

type rawJob struct {
	link string
	stage string
	name string
}

func findStage(scanner *bufio.Scanner) (string, error) {
	for scanner.Scan() {
		line := scanner.Bytes()
		if !bytes.HasPrefix(line, []byte("Stage: ")) {
			continue
		}
		return string(line[7:]), nil
	}
	return "", fmt.Errorf("Problem parsing job for field 'Stage'")
}

func findName(scanner *bufio.Scanner) (string, error) {
	for scanner.Scan() {
		line := scanner.Bytes()
		if !bytes.HasPrefix(line, []byte("Name: ")) {
			continue
		}
		return string(line[6:]), nil
	}
	return "", fmt.Errorf("Problem parsing job for field 'Name'")
}

func findJobs(scanner *bufio.Scanner, r *regexp.Regexp) ([]rawJob, error) {
	var jobs []rawJob
	for scanner.Scan() {
		line := scanner.Bytes()
		if !bytes.HasPrefix(line, []byte("Job #")) {
			continue
		}
		m := r.FindSubmatch(line)
		if m == nil {
			continue
		}
		job := string(m[1])
		stage, err := findStage(scanner)
		if err != nil {
			return nil, err
		}
		name, err := findName(scanner)
		if err != nil {
			return nil, err
		}
		jobs = append(jobs, rawJob{job, stage, name})
	}
	if len(jobs) == 0 {
		return nil, NotOursError
	}
	return jobs, nil
}

func findTriggeredBy(scanner *bufio.Scanner, r *regexp.Regexp) error {
	for scanner.Scan() {
		line := scanner.Bytes()
		if !bytes.HasPrefix(line, []byte("Pipeline #")) {
			continue
		}
		if !r.MatchString(string(line)) {
			continue
		}
		return nil
	}
	return NotOursError
}

// handleCusOpts returns an invalid custom option.
func handleCusOpts (scanner *bufio.Scanner, pat *regexp.Regexp) (string, error) {
	for scanner.Scan() {
		line := scanner.Bytes()
		if customOptsSectionCookie.Match(line) {
			break
		}
	}
	if !scanner.Scan() {
		return "", nil
	}
	if line := scanner.Bytes();
	// "The following options might have problems:"
	!bytes.HasPrefix(line, []byte("The following options")) {
		return "", nil
	}
	for scanner.Scan() {
		line := scanner.Bytes()
		if bytes.HasPrefix(line, []byte("DETAILS")) {
			break
		}
		if bytes.HasPrefix(line, []byte("-----")) {
			break
		}
		if bytes.HasPrefix(line, []byte("variable: ")) {
			s := string(line[10:])
			if pat.MatchString("lisp/" + s) {
				return "misc/test-custom-opts(variable: " + s + ")", nil
			}
		}
	}
	return "", nil
}


var getDocumentFunc func (url string) ([]byte, error) = feed.FetchURL

// findFailed returns the first failed ERT test
func findFailed(r io.Reader, pat *regexp.Regexp) (string, error) {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Bytes()
		if bytes.HasPrefix(line, []byte("SUMMARY OF TEST RESULTS")) {
			break
		}
	}
	if !scanner.Scan() {
		return "", nil
	}
	if line := scanner.Bytes();
	!bytes.HasPrefix(line, []byte("-----------------------")) {
		return "", nil
	}
	for scanner.Scan() {
		line := scanner.Bytes()
		if bytes.HasPrefix(line, []byte("DETAILS")) {
			break
		}
		if bytes.HasPrefix(line, []byte("-----")) {
			break
		}
		if bytes.HasPrefix(line, []byte(" ")) &&
			bytes.HasSuffix(line, []byte(".log")) {
			if pat.Match(line) {
				return string(bytes.TrimLeft(line, " ")), nil
			}
		}
		if bytes.HasPrefix(line, []byte("Contents of unfinished file ")) &&
			customOptsPat.Match(line[28:]) {
			return handleCusOpts(scanner, pat)
		}
	}
	return "", nil
}

func scrape(r io.Reader, rawDate string, pat *regexp.Regexp) ([]byte, error) {
	scanner := bufio.NewScanner(r)
	if err := findTriggeredBy(scanner, patTed); err != nil {
		return nil, err
	}
	jobs, err := findJobs(scanner, patRaw)
	if err != nil {
		return nil, err
	}
	var job rawJob
	var failedTest string
	for _, j := range jobs {
		if !strings.HasPrefix(j.name, "test-") {
			continue
		}
		doc, err := getDocumentFunc(j.link)
		if err != nil {
			return nil, err
		}
		ft, err := findFailed(bytes.NewReader(doc), pat)
		if err != nil {
			return nil, err
		}
		// Not ours
		if ft == "" {
			continue
		}
		failedTest = ft
		job = j
		break
	}
	if job == (rawJob{}) {
		return nil, NotOursError
	}
	date, err := renderDate(rawDate)
	if err != nil {
		return nil, err
	}
	out := []byte(
		`~ %BOLD` + job.name + " (" + job.stage + ") failed" +
			`%NORMAL ~ ` + date + ` ~ ` + failedTest + ` ● ` + job.link,
	)
	return out, nil
}

// handleBuildStatus composes a message from an emacs-buildstatus report
func handleBuildStatus(
	msg *mail.Message, rawDate string, pat *regexp.Regexp,
) ([]byte, error) {
	if strings.HasPrefix(rawDate, "Date: ") {
		rawDate = rawDate[6:]
	}
	return scrape(msg.Body, rawDate, pat)
}
