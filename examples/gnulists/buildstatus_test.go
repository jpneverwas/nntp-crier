package main

import (
	"fmt"
	"net/mail"
	"os"
	"path/filepath"
	"regexp"
	"testing"
)

var origDocFunc = getDocumentFunc
var url2Log = map[string]string {
	// bs_foreign.email
	"https://emba.gnu.org/emacs/emacs/-/jobs/53647/raw": "bs_tramp.log",
	"https://emba.gnu.org/emacs/emacs/-/jobs/53639/raw": "bs_tramp.log",
	// bs_erc.email
	"https://emba.gnu.org/emacs/emacs/-/jobs/53756/raw": "bs_erc.log",
	"https://emba.gnu.org/emacs/emacs/-/jobs/63072/raw": "bs_erc_native_23254.log",
	// bs_incomplete.email
	"https://emba.gnu.org/emacs/emacs/-/jobs/53834/raw": "bs_incomplete.log",
	"https://emba.gnu.org/emacs/emacs/-/jobs/53826/raw": "bs_incomplete.log",
	// bs_other.email
	"https://emba.gnu.org/emacs/emacs/-/jobs/54123/raw": "bs_other1.log",
	"https://emba.gnu.org/emacs/emacs/-/jobs/54115/raw": "bs_other2.log",
	// bs cusopt_erc.email
	"https://emba.gnu.org/emacs/emacs/-/jobs/67649/raw": "bs_cusopt_erc.log",
	"https://emba.gnu.org/emacs/emacs/-/jobs/67655/raw": "bs_cusopt_erc.log",
}

func docFuncReset() {
	getDocumentFunc = origDocFunc
}

func getDocumentTestFunc(url string) ([]byte, error) {
	fileName, ok := url2Log[url]
	if !ok {
		return nil, fmt.Errorf("%s not found", url)
	}
	contents, err := os.ReadFile(filepath.Join("testdata", fileName))
	if err != nil {
		return nil, err
	}
	return contents, nil
}

const wantGood = "~ %BOLDtest-all-inotify (normal) failed%NORMAL ~ "+
	"Sat, Sep 4 2021 16:46:03 +0000 ~ "+
	"lisp/erc/erc-scenarios-internal.log ● "+
	"https://emba.gnu.org/emacs/emacs/-/jobs/53756/raw"


func TestScrapeGood(t *testing.T) {
	getDocumentFunc = getDocumentTestFunc
	t.Cleanup(docFuncReset)
	fd, err := os.Open("testdata/bs_erc.email")
	if err != nil {
		t.Fatal(err)
	}
	p := buildStatusPat
	msg, err := mail.ReadMessage(fd)
	result, err := scrape(msg.Body, "Sat, 04 Sep 2021 16:46:03 +0000", p)
	if err != nil {
		t.Fatal(err)
	}
	if string(result) != wantGood {
		t.Fatalf("Unexpected result. Wanted: %s, got: %s", wantGood, result)
	}
}

func TestScrapeGoodPlus(t *testing.T) {
	getDocumentFunc = getDocumentTestFunc
	t.Cleanup(docFuncReset)
	fd, err := os.Open("testdata/bs_ercmore.email")
	if err != nil {
		t.Fatal(err)
	}
	p := buildStatusPat
	msg, err := mail.ReadMessage(fd)
	result, err := scrape(msg.Body, "Sat, 04 Sep 2021 16:46:03 +0000", p)
	if err != nil {
		t.Fatal(err)
	}
	if string(result) != wantGood {
		t.Fatalf("Unexpected result. Wanted: %s, got: %s", wantGood, result)
	}
}

const wantNative = "~ %BOLDtest-native-comp-speed2 (native-comp) failed%NORMAL ~ "+
"Sat, Sep 4 2021 16:46:03 +0000 ~ lisp/erc/erc-scenarios-base-reuse-buffers.log "+
"● https://emba.gnu.org/emacs/emacs/-/jobs/63072/raw"

func TestScrapeNative23254(t *testing.T) {
	getDocumentFunc = getDocumentTestFunc
	t.Cleanup(docFuncReset)
	fd, err := os.Open("testdata/bs_erc_native_23254.email")
	if err != nil {
		t.Fatal(err)
	}
	p := buildStatusPat
	msg, err := mail.ReadMessage(fd)
	result, err := scrape(msg.Body, "Sat, 04 Sep 2021 16:46:03 +0000", p)
	if err != nil {
		t.Fatal(err)
	}
	if string(result) != wantNative {
		t.Fatalf("Unexpected result. Wanted: %s, got: %s", wantNative, result)
	}
}

func TestScrapeForeign(t *testing.T) {
	getDocumentFunc = getDocumentTestFunc
	t.Cleanup(docFuncReset)
	fd, err := os.Open("testdata/bs_foreign.email")
	if err != nil {
		t.Fatal(err)
	}
	p := buildStatusPat
	msg, err := mail.ReadMessage(fd)
	result, err := scrape(msg.Body, "Sat, 04 Sep 2021 16:46:03 +0000", p)
	if err != NotOursError {
		t.Fatalf("Unexpected result. Expected error, got: %s", result)
	}
	t.Log(err)
}

func TestScrapeIncomplete(t *testing.T) {
	getDocumentFunc = getDocumentTestFunc
	t.Cleanup(docFuncReset)
	fd, err := os.Open("testdata/bs_incomplete.email")
	if err != nil {
		t.Fatal(err)
	}
	p := buildStatusPat
	msg, err := mail.ReadMessage(fd)
	result, err := scrape(msg.Body, "Sat, 04 Sep 2021 16:46:03 +0000", p)
	if err != NotOursError {
		t.Fatalf("Unexpected result. Expected error, got: %s", result)
	}
	t.Log(err)
}

const wantOther = "~ %BOLDtest-native-comp-speed0 (native-comp) "+
	"failed%NORMAL ~ "+
	"Sun, Sep 18 2022 23:03:54 -0400 ~ misc/test-custom-opts.log ● "+
	"https://emba.gnu.org/emacs/emacs/-/jobs/54123/raw"

func TestScrapeOther(t *testing.T) {
	getDocumentFunc = getDocumentTestFunc
	t.Cleanup(docFuncReset)
	fd, err := os.Open("testdata/bs_other.email")
	if err != nil {
		t.Fatal(err)
	}
	p := regexp.MustCompile(".+")
	msg, err := mail.ReadMessage(fd)
	result, err := scrape(msg.Body, "Sun, 18 Sep 2022 23:03:54 -0400", p)
	if err != nil {
		t.Fatal(err)
	}
	if string(result) != wantOther {
		t.Fatalf("Unexpected result. Wanted: %s, got: %s", wantOther, result)
	}
	t.Log(err)
}

const wantCusOpt = "~ %BOLDtest-all-inotify (normal) "+
	"failed%NORMAL ~ Thu, May 18 2023 11:40:15 -0400 ~ "+
	"misc/test-custom-opts(variable: erc-fill-spaced-commands)"+
	" ● https://emba.gnu.org/emacs/emacs/-/jobs/67649/raw"

func TestScrapeCusOpt(t *testing.T) {
	getDocumentFunc = getDocumentTestFunc
	t.Cleanup(docFuncReset)
	fd, err := os.Open("testdata/bs_cusopt_erc.email")
	if err != nil {
		t.Fatal(err)
	}
	p := buildStatusPat
	msg, err := mail.ReadMessage(fd)
	result, err := scrape(msg.Body, "Thu, 18 May 2023 11:40:15 -0400", p)
	if err != nil {
		t.Fatal(err)
	}
	if string(result) != wantCusOpt {
		t.Fatalf("Unexpected result. Wanted: %s, got: %s", wantCusOpt, result)
	}
}

