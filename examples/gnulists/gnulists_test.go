package main

import (
	"bufio"
	"bytes"
	"net"
	"net/mail"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"testing"
	"time"

	helpers "gitlab.com/jpneverwas/nntp-crier/internal/testing"
)

var fakeStatPat = `lisp/fake|doc/lispref/files\.texi`
var fakeStatPatRegexp = regexp.MustCompile(fakeStatPat)

func TestParseTarget(t *testing.T) {
	s, err := parseTarget("#chan", "emacs-erc")
	if err != nil {
		t.Fatal(err)
	}
	if s != "#chan" {
		t.Fatalf("Wanted #chan, got %s", s)
	}
	// Not found
	s, err = parseTarget("foo=#chan,bar=#spam", "emacs-erc")
	if err == nil {
		t.Fatalf("Wanted error, got %s", s)
	}
	// Found
	s, err = parseTarget("foo=#chan,gmane.test=#spam", "gmane.test")
	if err != nil {
		t.Fatal(err)
	}
	if s != "#spam" {
		t.Fatalf("Wanted #spam, got %s", s)
	}
	// Fallback
	s, err = parseTarget("foo=#chan,bar=#spam,default=#test", "gmane.test")
	if err != nil {
		t.Fatal(err)
	}
	if s != "#test" {
		t.Fatalf("Wanted #test, got %s", s)
	}
}

func TestHandleDiff(t *testing.T) {
	if !fakeStatPatRegexp.Match([]byte(" doc/lispref/files.texi | 4 ++++")) {
		t.Errorf("Basic Pattern flawed") // regression (oops)
	}
	// Not ours
	f, err := os.Open("testdata/diff.email")
	if err != nil {
		t.Fatal(err)
	}
	msg, err := mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	date := "Date: Thu,  8 Jul 2021 15:14:37 -0400 (EDT)"
	output, err := handleDiff(msg, date, diffStatPat)
	if err != NotOursError {
		t.Error("Did not raise NotOursError")
	}
	//
	if _, err = f.Seek(0, 0); err != nil {
		t.Error(err)
	}
	msg, err = mail.ReadMessage(f)
	if err != nil {
		t.Fatal(err)
	}
	output, err = handleDiff(msg, date, fakeStatPatRegexp)
	if err != nil {
		t.Error(err)
	}
	expect, err := os.ReadFile("testdata/diff.expect")
	if err != nil {
		t.Fatal(err)
	}
	if err := helpers.Compare(expect, output); err != nil {
		t.Fatal(err)
	}
}

func startServer(addr string, done chan string) {
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		panic(err)
	}
	conn, err := ln.Accept()
	if err != nil {
		panic(err)
	}
	msg, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		panic(err)
	}
	done <- msg
}

func TestSendOutput(t *testing.T) {
	got := make(chan string)
	go startServer(":54321", got)
	time.Sleep(time.Millisecond * 100)
	sendOutput("#chan", ":54321", []byte("Hi"))
	response := <-got
	if response != "#chan Hi\n" {
		t.Fatalf("Unexpected: %s", response)
	}
}

func populateFakeBin(fakeBin, url string) error {
	if err := os.Mkdir(fakeBin, 0775); err != nil {
		return err
	}

	fakeId2mm := filepath.Join(fakeBin, "id2mm")
	fakeCompose := filepath.Join(fakeBin, "compose")

	var id2mmSrc = "#!/bin/sh\nprintf %s " + url + "\n"
	if err := os.WriteFile(fakeId2mm, []byte(id2mmSrc), 0700); err != nil {
		return err
	}

	composeDir, err := filepath.Abs("../../cmd/compose/")
	if err != nil {
		return err
	}
	var composeSrc = "#!/bin/sh\nset -e\ncd " + composeDir +
		"\nexec go run . \"$@\"\n"
	if err := os.WriteFile(fakeCompose, []byte(composeSrc), 0700); err != nil {
		return err
	}
	return nil
}

func TestFetchUrl(t *testing.T) {
	fakeBin := filepath.Join(t.TempDir(), "bin")
	origPath := os.Getenv("PATH")
	if err := os.Setenv("PATH", fakeBin+":"+os.Getenv("PATH")); err != nil {
		t.Error(err)
	}
	if err := populateFakeBin(fakeBin, "$@"); err != nil {
		t.Error(err)
	}
	got, err := fetchUrl(
		"help-gnu-emacs", "Message-Id: <87y293sdxk.fsf@zoho.eu>",
	)
	if err != nil {
		t.Error(err)
	}
	if string(got) != "-listhelp-gnu-emacs-id<87y293sdxk.fsf@zoho.eu>" {
		t.Errorf("Unexpected value from id2mm: %s", got)
	}
	if err := os.Setenv("PATH", origPath); err != nil {
		t.Error(err)
	}
}

var argsDiff = []string{
	"gmane.emacs.diffs",
	"Subject: master 4bfdf8c: " +
		"* doc/lispref/files.texi (Magic File Names): " +
		"Add make-lock-file-name.",
	"From: Michael.Albinus@gmx.de (Michael Albinus)",
	"Date: Thu,  8 Jul 2021 15:14:37 -0400 (EDT)",
	"Message-Id: <20210708191437.ADD1920B72@vcs0.savannah.gnu.org>",
	":bytes 4719",
	":lines 41",
	"Xref:full Xref: news.gmane.io gmane.emacs.diffs:165064",
}

// Unlike the tests in cmd/compose/compose_tests.go, these must be run in one
// stretch. Basically, each requires a populated database.

// This is a punt
func diffNoMatch(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "DiffNoMatch" {
		os.Args = append(os.Args[:1], argsDiff...)
		main()
		os.Exit(0)
	}
	src, err := os.ReadFile("testdata/diff.email")
	if err != nil {
		t.Fatal(err)
	}
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(os.Environ(), "GO_WANT_HELPER_PROCESS=DiffNoMatch")
	output, err := cmd.Output()
	if err == nil {
		t.Fatal("Did not error when expected")
	}
	if ec := cmd.ProcessState.ExitCode(); ec != 31 {
		t.Fatalf("Unexpected exit code: %d, saw: %q", ec, output)
	}
	if len(output) != 0 {
		t.Fatal("Unexpected output")
	}
}

// Defend against false positive in body
func diffNoMatchPathBody(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "DiffNoMatchPathBody" {
		os.Args = append(os.Args[:1], argsDiff...)
		main()
		os.Exit(0)
	}
	src, err := os.ReadFile("testdata/diff_pathbody.email")
	if err != nil {
		t.Fatal(err)
	}
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(os.Environ(), "GO_WANT_HELPER_PROCESS=DiffNoMatchPathBody")
	output, err := cmd.Output()
	if err == nil {
		t.Fatal("Did not error when expected")
	}
	if ec := cmd.ProcessState.ExitCode(); ec != 31 {
		t.Fatalf("Unexpected exit code: %d, saw: %q", ec, output)
	}
	if len(output) != 0 {
		t.Fatal("Unexpected output")
	}
}

func diffMatch(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "DiffMatch" {
		os.Args = append(os.Args[:1], argsDiff...)
		main()
		os.Exit(0)
	}
	src, err := os.ReadFile("testdata/diff.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"GO_WANT_HELPER_PROCESS=DiffMatch",
		"DIFF_STAT_PAT="+fakeStatPat,
		"IRCCAT_DEBUG=1",
	)
	output, err := cmd.Output()
	if err != nil {
		if ee, ok := err.(*exec.ExitError); ok {
			t.Fatalf("Failed to spawn child process: %v, %q", err, ee.Stderr)
		}
		t.Fatalf("Failed to spawn child process: %v", err)
	}
	expect, err := os.ReadFile("testdata/diff.expect")
	if err := helpers.Compare(expect, output); err != nil {
		t.Fatal(err)
	}
}

var argsDiff27 = []string{
	"gmane.emacs.diffs",
	"Subject: emacs-27 b77f6af: ERC right stamps: also use " +
		"latest buffer's window's width (Bug#44140)",
	"From: bandali@gnu.org (Amin Bandali)",
	"Date: Fri,  6 Aug 2021 01:16:03 -0400 (EDT)",
	"Message-Id: <20210806051603.9DA8920BD3@vcs0.savannah.gnu.org>",
	"References: <20210806051601.7440.90873@vcs0.savannah.gnu.org>",
	":bytes 8522",
	":lines 108",
	"Xref:full Xref: news.gmane.io gmane.emacs.diffs:165540",
}

func diff27NoMatch(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "Diff27NoMatch" {
		os.Args = append(os.Args[:1], argsDiff27...)
		main()
		os.Exit(0)
	}
	src, err := os.ReadFile("testdata/diff_27.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"GO_WANT_HELPER_PROCESS=Diff27NoMatch",
		"DIFF_STAT_PAT="+fakeStatPat,
		"IRCCAT_DEBUG=1",
	)
	output, err := cmd.Output()
	if err == nil {
		t.Fatal("Did not error when expected")
	}
	if ec := cmd.ProcessState.ExitCode(); ec != 31 {
		t.Fatalf("Unexpected exit code: %d, saw: %q", ec, output)
	}
	if len(output) != 0 {
		t.Fatal("Unexpected output")
	}
}

func diff27Match(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "Diff27Match" {
		os.Args = append(os.Args[:1], argsDiff27...)
		main()
		os.Exit(0)
	}
	src, err := os.ReadFile("testdata/diff_27.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"GO_WANT_HELPER_PROCESS=Diff27Match",
		"IRCCAT_DEBUG=1",
	)
	output, err := cmd.Output()
	if err != nil {
		if ee, ok := err.(*exec.ExitError); ok {
			t.Fatalf("Failed to spawn child process: %v, %q", err, ee.Stderr)
		}
		t.Fatalf("Failed to spawn child process: %v", err)
	}
	expect, err := os.ReadFile("testdata/diff_27.expect")
	if err := helpers.Compare(expect, output); err != nil {
		t.Fatal(err)
	}
}

var argsDiffPushed = []string{
	"gmane.emacs.diffs",
	"Subject: branch scratch/icomplete-lazy-highlight-attempt-2 " +
		"created (now 70e5147)",
	"From: =?UTF-8?Q?Jo=C3=A3o_T=C3=A1vora?= <joaotavora@gmail.com>",
	"Date: Sat, 14 Aug 2021 19:44:08 -0400 (EDT)",
	"Message-Id: <20210814234408.29424.39182@vcs0.savannah.gnu.org>",
	"References:",
	":bytes 3474",
	":lines 10",
	"Xref:full Xref: news.gmane.io gmane.emacs.diffs:165676",
}

func diffPushed(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "DiffPushed" {
		os.Args = append(os.Args[:1], argsDiffPushed...)
		main()
		os.Exit(0)
	}
	src, err := os.ReadFile("testdata/diff_pushed.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"GO_WANT_HELPER_PROCESS=DiffPushed",
		"DIFF_STAT_PAT="+fakeStatPat,
		"IRCCAT_DEBUG=1",
	)
	output, err := cmd.Output()
	if err == nil {
		t.Fatal("Did not error when expected")
	}
	if ec := cmd.ProcessState.ExitCode(); ec != 31 {
		t.Fatalf("Unexpected exit code: %d, saw: %q", ec, cmd.Stderr)
	}
	if len(output) != 0 {
		t.Fatal("Unexpected output")
	}
}

var argsDiffMerged = []string{
	"gmane.emacs.diffs",
	"Subject: master 958f591: Merge from origin/emacs-27",
	"From: rgm@gnu.org (Glenn Morris)",
	"Date: Mon, 16 Aug 2021 11:01:12 -0400 (EDT)",
	"Message-Id: <20210816150112.449D620B72@vcs0.savannah.gnu.org>",
	"References: <20210816150110.1160.76982@vcs0.savannah.gnu.org>",
	":bytes 6170",
	":lines 84",
	"Xref:full Xref: news.gmane.io gmane.emacs.diffs:165728",
}

func diffMerged(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "DiffMerged" {
		os.Args = append(os.Args[:1], argsDiffMerged...)
		main()
		os.Exit(0)
	}
	src, err := os.ReadFile("testdata/diff_merged.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"GO_WANT_HELPER_PROCESS=DiffMerged",
		"DIFF_STAT_PAT="+fakeStatPat,
		"IRCCAT_DEBUG=1",
	)
	output, err := cmd.Output()
	if err == nil {
		t.Fatal("Did not error when expected")
	}
	if ec := cmd.ProcessState.ExitCode(); ec != 31 {
		t.Fatalf("Unexpected exit code: %d, saw: %q", ec, cmd.Stderr)
	}
	if len(output) != 0 {
		t.Fatal("Unexpected output")
	}
}

var argsBase = []string{
	"gmane.emacs.devel",
	"Subject: Re: Highlight current line when using gud",
	"From: Eli Zaretskii <eliz@gnu.org>",
	"Date: Sun, 04 Jul 2021 07:30:29 +0300",
	"Message-Id: <838s2mitui.fsf@gnu.org>",
	"References: <46933061-DFBC-406C-8489-F3DC7EE770BA@kuaishou.com> " +
		"<CAHO21CJQ+qqcJ7SYmEHbmBdEcNSvuRooFvp4vxwDOgp2qj7iFw@mail.gmail.com> " +
		"<87sg0z8t5t.fsf@gnus.org> ",
	":bytes 4052",
	":lines 20",
	"Xref:full: Xref: news.gmane.io gmane.emacs.devel:271120",
}

func baseNoMatch(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "BaseNoMatch" {
		os.Args = append(os.Args[:1], argsBase...)
		main()
		os.Exit(0)
	}
	// Create fake bin
	fakeBin := filepath.Join(t.TempDir(), "bin")
	if err := populateFakeBin(fakeBin, "\nexit 42"); err != nil {
		t.Error(err)
	}
	// Run superior
	src, err := os.ReadFile("testdata/base.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"PATH="+fakeBin+":"+os.Getenv("PATH"),
		"GO_WANT_HELPER_PROCESS=BaseNoMatch",
		"IRCCAT_DEBUG=1",
	)
	_, err = cmd.Output()
	if err == nil {
		t.Fatal("Did not generate expected error")
	}
	ee, ok := err.(*exec.ExitError)
	if !ok {
		t.Fatal("Did not generate expected error type")
	}
	if ec := ee.ExitCode(); ec != 42 {
		t.Fatalf("Unexpected exit code: %d", ec)
	}
}

var argsBaseNoMatchGmane = []string{
	"gmane.emacs.bugs",
	"Subject: bug#54536: 29.0.50; Improve ERC's handling of multiline prompt input",
	"From: \"J.P.\" <jp@neverwas.me>",
	"Date: Tue, 17 May 2022 18:34:10 -0700",
	"Message-Id: <87ilq3r5d9.fsf__45053.505238478$1652837716$gmane$org@neverwas.me>",
	"References: <87k0ckg5pn.fsf@neverwas.me> <874k1os3te.fsf@neverwas.me>",
	":bytes 6043",
	":lines 12",
	"Xref:full Xref: news.gmane.io gmane.emacs.bugs:232514",
}

func baseNoMatchGmane(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "BaseNoMatchGmane" {
		os.Args = append(os.Args[:1], argsBaseNoMatchGmane...)
		main()
		os.Exit(0)
	}
	// Run superior
	src, err := os.ReadFile("testdata/base_gmane.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"GO_WANT_HELPER_PROCESS=BaseNoMatchGmane",
		"IRCCAT_DEBUG=1",
	)
	_, err = cmd.Output()
	if err == nil {
		t.Fatal("Did not generate expected error")
	}
	ee, ok := err.(*exec.ExitError)
	if !ok {
		t.Fatal("Did not generate expected error type")
	}
	if ec := ee.ExitCode(); ec != 31 {
		t.Fatalf("Failed to spawn child process: %v, %q", err, ee.Stderr)
	}
}

func baseMatch(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "BaseMatch" {
		os.Args = append(os.Args[:1], argsBase...)
		main()
		os.Exit(0)
	}
	// Create fake bin
	dir := t.TempDir()
	fakeBin := filepath.Join(dir, "bin")
	if err := populateFakeBin(
		fakeBin, "http://quimby.gnus.org/pipermail/gmane-test/12345",
	); err != nil {
		t.Error(err)
	}
	// Run it
	src, err := os.ReadFile("testdata/base.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"PATH="+fakeBin+":"+os.Getenv("PATH"),
		"GO_WANT_HELPER_PROCESS=BaseMatch",
		"IRCCAT_DEBUG=1",
	)
	output, err := cmd.Output()
	if err != nil {
		if ee, ok := err.(*exec.ExitError); ok {
			t.Fatalf("Failed to spawn child process: %v, %q", err, ee.Stderr)
		}
		t.Fatalf("Failed to spawn child process: %v", err)
	}
	expect, err := os.ReadFile("testdata/base.expect")
	if err := helpers.Compare(expect, output); err != nil {
		t.Fatal(err)
	}
}

func baseMatchIO(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "BaseMatchIO" {
		os.Args = append(os.Args[:1], argsBase...)
		main()
		os.Exit(0)
	}
	// Start server
	got := make(chan string)
	go startServer(":23456", got)
	time.Sleep(time.Millisecond * 10)
	// Create fake bin
	dir := t.TempDir()
	fakeBin := filepath.Join(dir, "bin")
	if err := populateFakeBin(
		fakeBin, "http://quimby.gnus.org/pipermail/gmane-test/12345",
	); err != nil {
		t.Error(err)
	}
	// Run it
	src, err := os.ReadFile("testdata/base.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"PATH="+fakeBin+":"+os.Getenv("PATH"),
		"GO_WANT_HELPER_PROCESS=BaseMatchIO",
		"IRCCAT_TARGET=#chan",
		"IRCCAT_LISTEN_PORT=23456",
	)
	err = cmd.Run()
	response := <-got
	if err != nil {
		t.Fatalf("Failed to spawn child process: %v", err)
	}
	if !strings.HasPrefix(response, "#chan ") {
		t.Fatalf("Unexpected: %s", response)
	}
	if !strings.HasSuffix(response, "\n") {
		t.Fatalf("Unexpected: %s", response)
	}
	output := []byte(response[6 : len(response)-1])
	expect, err := os.ReadFile("testdata/base.expect")
	if err := helpers.Compare(expect, output); err != nil {
		t.Fatal(err)
	}
}

var argsQP = []string{
	"gmane.emacs.erc.general",
	"Subject: bug#48598: Strange ERC/ZNC Bug/Problem",
	"From: acdw <acdw@acdw.net>",
	"Date: Sat, 04 Sep 2021 16:46:03 +0000",
	"Message-Id: <8537a530-66b8-4325-86af-272f1cbf716d@www.fastmail.com>",
	"References: <875yzakzvi.fsf@neverwas.me>",
	":bytes 9131",
	":lines 52",
	"Xref:full Xref: news.gmane.io gmane.emacs.bugs:213422 gmane.emacs.erc.general:1581",
}

func qpMatch(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "QPMatch" {
		os.Args = append(os.Args[:1], argsQP...)
		main()
		os.Exit(0)
	}
	// Create fake bin
	dir := t.TempDir()
	fakeBin := filepath.Join(dir, "bin")
	if err := populateFakeBin(
		fakeBin, "https://lists.gnu.org/archive/html/bug-gnu-emacs/2021-09/msg00283.html",
	); err != nil {
		t.Error(err)
	}
	// Run it
	src, err := os.ReadFile("testdata/qp.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"PATH="+fakeBin+":"+os.Getenv("PATH"),
		"GO_WANT_HELPER_PROCESS=QPMatch",
		"IRCCAT_DEBUG=1",
	)
	output, err := cmd.Output()
	if err != nil {
		if ee, ok := err.(*exec.ExitError); ok {
			t.Fatalf("Failed to spawn child process: %v, %q", err, ee.Stderr)
		}
		t.Fatalf("Failed to spawn child process: %v", err)
	}
	expect, err := os.ReadFile("testdata/qp.expect")
	if err := helpers.Compare(expect, output); err != nil {
		t.Fatal(err)
	}
}

func diffDeleteMatch(t *testing.T) {
	origmsgSub := diffMsgSubject
	diffMsgSubject = regexp.MustCompile(`\b(fixes|features)\b`)
	t.Cleanup(func() {
		diffMsgSubject = origmsgSub
	})
	if os.Getenv("GO_WANT_HELPER_PROCESS") == "DiffDeleteMatch" {
		os.Args = append(os.Args[:1], argsDiff...)
		main()
		os.Exit(0)
	}
	src, err := os.ReadFile("testdata/diff_cmpcust_delete.email")
	cmd := exec.Command(os.Args[0], "-test.run=TestAll")
	cmd.Stdin = bytes.NewBuffer(src)
	cmd.Env = append(
		os.Environ(),
		"GO_WANT_HELPER_PROCESS=DiffDeleteMatch",
		"IRCCAT_DEBUG=1",
	)
	output, err := cmd.Output()
	if err != nil {
		if ee, ok := err.(*exec.ExitError); ok {
			t.Fatalf("Failed to spawn child process: %v, %q", err, ee.Stderr)
		}
		t.Fatalf("Failed to spawn child process: %v", err)
	}
	expect, err := os.ReadFile("testdata/diff_cmpcust_delete.expect")
	if err := helpers.Compare(expect, output); err != nil {
		t.Fatal(err)
	}
}

func all(t *testing.T) {
	t.Run("DiffNoMatch", diffNoMatch)
	t.Run("DiffNoMatchPathBody", diffNoMatchPathBody)
	t.Run("DiffMatch", diffMatch)
	t.Run("Diff27NoMatch", diff27NoMatch)
	t.Run("Diff27Match", diff27Match)
	t.Run("DiffPushed", diffPushed)
	t.Run("DiffMerged", diffMerged)
	t.Run("DiffDeleteMatch", diffDeleteMatch)
	t.Run("BaseNoMatch", baseNoMatch)
	t.Run("BaseNoMatchGmane", baseNoMatchGmane)
	t.Run("BaseMatch", baseMatch)
	t.Run("BaseMatchIO", baseMatchIO)
	t.Run("QPMatch", qpMatch)
}

func TestAll(t *testing.T) {
	switch os.Getenv("GO_WANT_HELPER_PROCESS") {
	default:
		all(t)
	case "DiffNoMatch":
		t.Run("DiffNoMatch", diffNoMatch)
	case "DiffNoMatchPathBody":
		t.Run("DiffNoMatchPathBody", diffNoMatchPathBody)
	case "DiffMatch":
		t.Run("DiffMatch", diffMatch)
	case "Diff27NoMatch":
		t.Run("Diff27NoMatch", diff27NoMatch)
	case "Diff27Match":
		t.Run("Diff27Match", diff27Match)
	case "DiffPushed":
		t.Run("DiffPushed", diffPushed)
	case "DiffMerged":
		t.Run("DiffMerged", diffMerged)
	case "DiffDeleteMatch":
		t.Run("DiffDeleteMatch", diffDeleteMatch)
	case "BaseNoMatch":
		t.Run("BaseNoMatch", baseNoMatch)
	case "BaseNoMatchGmane":
		t.Run("BaseNoMatchGmane", baseNoMatchGmane)
	case "BaseMatch":
		t.Run("BaseMatch", baseMatch)
	case "BaseMatchIO":
		t.Run("BaseMatchIO", baseMatchIO)
	case "QPMatch":
		t.Run("QPMatch", qpMatch)
	}
}
